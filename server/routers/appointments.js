const express = require('express')
const { asyncHandler } = require('../helpers/errors')
const { setModel, ObjectId } = require('../helpers/models');
const { prepare, prepareModel, getQuery, log, setUpdated } = require('../helpers/utils');
const router = express.Router()

// TODO: Remove services and add services.length projection to this...
const findAll = asyncHandler(async (req, res) => { // handle query params, with _page and _limit
  console.log(`GET /appointments`, req.query)
  const Appointment = req.model
  const q = getQuery(req.query)
  const [ items, count ] = await Promise.all([
    Appointment.find(q.filter).sort(q.sort).skip(q.skip).limit(q.limit).lean(),
    Appointment.countDocuments(q.filter)
  ])
  res.setHeader('X-Total-Count', count)
  res.send(items.map(prepare))
})

const findOne = asyncHandler(async (req, res) => {
  console.log('GET /appointments/:id', req.params, req.query)
  const Appointment = req.model
  const appointment = await Appointment.findById(req.params.id).lean()
  if(!appointment){
    return res.status(400).send({ error: 'There is no appointment with that ID.'})
  }
  return res.send(prepare(appointment))
})

const updateOne = asyncHandler(async (req, res) => {
  console.log('PUT /appointments/:id')
  if(!req.params.id){
    return res.status(400).send({ error: 'Bad Request - missing ID'})
  }
  const Appointment = req.model
  const appointment = await Appointment.findById(req.params.id)
  appointment.set(req.body)
  const modifiedPaths = appointment.modifiedPaths()
  if(modifiedPaths.length){
    setUpdated(appointment, req)
    await appointment.save()
  }
  return res.send(prepareModel(appointment))
})

const deleteOne = asyncHandler(async (req, res) => {
  console.log(`DELETE /appointments/:id`, req.params)
  const Appointment = req.model
  const { id } = req.params
  await Appointment.deleteOne({ _id: ObjectId(id) })
  return res.send({ id })
})

const deleteMany = asyncHandler(async (req, res) => {
  console.log('POST /appointments/deleteMany', req.body)
  const Appointment = req.model
  const { ids } = req.body
  if(!ids.length){
    return res.status(400).send({ error: 'There were no IDs submitted for deletion'})
  }
  await Appointment.deleteMany({ _id: { $in: ids.map(ObjectId) }})
  return res.send(ids)
})

router.use(setModel('Appointment'))
router.route('/').get(findAll)
router.post('/deleteMany', express.json(), deleteMany)
router.route('/:id')
  .get(findOne)
  .put(express.json(), updateOne)
  .delete(deleteOne)

module.exports = router