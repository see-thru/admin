const express = require('express')
const { asyncHandler } = require('../helpers/errors')
const { setModel } = require('../helpers/models')
const { prepare, prepareModel, getQuery, log, setUpdated } = require('../helpers/utils');
const router = express.Router()

const findAll = asyncHandler(async (req, res) => {
  console.log(`GET /users`, req.query)
  const Model = req.model
  const q = getQuery(req.query, ['firstName', 'lastName', 'email', 'invitedBy'], false)
  const [ users, count ] = await Promise.all([
    Model.find(q.filter).sort(q.sort).skip(q.skip).limit(q.limit).lean(),
    Model.countDocuments(q.filter)
  ])
  res.setHeader('X-Total-Count', count)
  res.send(users.map(prepare))
})

const findOne = asyncHandler(async (req, res) => {
  console.log('GET /users/:id', req.params, req.query)
  const Model = req.model
  const user = await Model.findOne({ _id: req.params.id }).lean()
  if(!user){
    return res.status(400).send({ error: 'There is no user with that ID.'})
  }
  return res.send(prepare(user))
})

const updateOne = asyncHandler(async (req, res) => {
  console.log('PUT /users/:id', req.body)
  const Model = req.model
  if(!req.params.id){
    return res.status(400).send({ error: 'Bad Request - missing ID'})
  }
  const user = await Model.findById(req.params.id)
  user.set(req.body)
  const modifiedPaths = user.modifiedPaths()
  console.log('Modified:', modifiedPaths)
  if(modifiedPaths.length){
    setUpdated(user, req)
    await user.save()
  }
  return res.send(prepareModel(user))
})

const deleteOne = asyncHandler(async (req, res) => {
  console.log(`DELETE /users/:id`, req.params)
  const Model = req.model
  const { id } = req.params
  await Model.deleteOne({ _id: id })
  return res.send({ id })
})

const deleteMany = asyncHandler(async (req, res) => {
  console.log('POST /users/deleteMany', req.body)
  const Model = req.model
  const { ids } = req.body
  if(!ids.length){
    return res.status(400).send({ error: 'There were no IDs submitted for deletion'})
  }
  await Model.deleteMany({ _id: { $in: ids }})
  return res.send(ids)
})

router.use(setModel('User'))

router.route('/').get(findAll)
router.post('/deleteMany', express.json(), deleteMany)
router.route('/:id')
  .get(findOne)
  .put(express.json(), updateOne)
  .delete(deleteOne)

module.exports = router