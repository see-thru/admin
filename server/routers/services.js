const express = require('express')
const { prepare, prepareModel, getQuery, log, setUpdated } = require('../helpers/utils')
const { asyncHandler } = require('../helpers/errors')
const { setModel, ObjectId } = require('../helpers/models')
const router = express.Router()

const findAll = asyncHandler(async (req, res) => {
  log('GET /services', req.query)
  const Service = req.model
  const q = getQuery(req.query, ['name','includes'])
  const [ services, count ] = await Promise.all([
    Service.find(q.filter).sort(q.sort).skip(q.skip).limit(q.limit).lean(),
    Service.countDocuments(q.filter)
  ])
  res.setHeader('X-Total-Count', count)
  res.send(services.map(prepare))
})

const createOne = asyncHandler(async (req, res) => {
  log('POST /services', req.body)
  const Service = req.model
  const service = new Service(req.body)
  setUpdated(service, req)
  return service.save().then(saved => {
    return res.send(prepareModel(saved))
  })
})

const findOne = asyncHandler(async (req, res) => {
  log('GET /services/:id', req.params, req.query)
  const Service = req.model
  const service = await Service.findById(req.params.id).lean()
  if(!service){
    return res.status(400).send({ error: 'There is no service with that ID.'})
  }
  return res.send(prepare(service))
})

const updateOne = asyncHandler(async (req, res) => {
  log('PUT /services/:id', req.body)
  if(!req.params.id){
    return res.status(400).send({ error: 'Bad Request - missing ID'})
  }
  const Service = req.model
  const service = await Service.findById(req.params.id)
  service.set(req.body)
  const modifiedPaths = service.modifiedPaths()
  if(modifiedPaths.length){
    setUpdated(service, req)
    await service.save()
  }
  return res.send(prepareModel(service))
})

const deleteOne = asyncHandler(async (req, res) => {
  log(`DELETE /services/:id`, req.params)
  const Service = req.model
  const { id } = req.params
  await Service.deleteOne({ _id: ObjectId(id) })
  res.send({ id })
})

const deleteMany = asyncHandler(async (req, res) => {
  log('POST /services/deleteMany', req.body)
  const Service = req.model
  const { ids } = req.body
  if(!ids.length){
    return res.status(400).send({ error: 'There were no IDs submitted for deletion'})
  }
  await Service.deleteMany({ _id: { $in: ids.map(ObjectId) }})
  res.send(ids)
})

router.use(setModel('Service'))

router.route('/')
  .get(findAll)
  .post(express.json(), createOne)

router.post('/deleteMany', express.json(), deleteMany)

router.route('/:id')
  .get(findOne)
  .put(express.json(), updateOne)
  .delete(deleteOne)

module.exports = router
