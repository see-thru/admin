const express = require('express')
const { prepare, getQuery, log } = require('../helpers/utils')
const { asyncHandler } = require('../helpers/errors')
const { setModel } = require('../helpers/models')
const router = express.Router()

const findAll = asyncHandler(async (req, res) => { // handle query params, with _page and _limit
  log('GET /actions', req.query)
  const Action = req.model
  const q = getQuery(req.query, ['action','detail','link'])
  const [ items, count ] = await Promise.all([
    Action.find(q.filter).sort(q.sort).skip(q.skip).limit(q.limit).lean(),
    Action.countDocuments(q.filter)
  ])
  res.setHeader('X-Total-Count', count)
  res.send(items.map(prepare))
})

const findOne = asyncHandler(async (req, res) => {
  log('GET /actions/:id', req.params, req.query)
  const Action = req.model
  const item = await Action.findById(req.params.id).lean()
  if(!item){
    return res.status(400).send({ error: 'There is no action with that ID.'})
  }
  return res.send(prepare(item))
})

router.use(setModel('Action'))
router.route('/').get(findAll)
router.route('/:id').get(findOne)

module.exports = router