const express = require('express')
const { asyncHandler } = require('../helpers/errors')
const { setModel, ObjectId } = require('../helpers/models')
const { prepare, prepareModel, cleanServices, getQuery, log, setUpdated } = require('../helpers/utils');
const router = express.Router()

const findAll = asyncHandler(async (req, res) => {
  console.log(`GET /serviceLists`, req.query)
  const ServiceList = req.model
  const q = getQuery(req.query)
  const [ items, count ] = await Promise.all([
    ServiceList.find(q.filter).sort(q.sort).skip(q.skip).limit(q.limit).lean(),
    ServiceList.countDocuments(q.filter)
  ])
  res.setHeader('X-Total-Count', count)
  res.send(items.map(prepare))
})

const findOne = asyncHandler(async (req, res) => {
  console.log('GET /serviceLists/:id', req.params, req.query)
  const ServiceList = req.model
  const list = await ServiceList.findById(req.params.id).lean()
  if(!list){
    return res.status(400).send({ error: 'There is no service list with that ID.'})
  }
  return res.send(prepare(list))
})

const createOne = asyncHandler(async (req, res) => {
  log('POST /serviceLists', req.body)
  const ServiceList = req.model
  const list = cleanServices(new ServiceList(req.body))
  setUpdated(list, req)
  return list.save().then(saved => {
    return res.send(prepareModel(saved))
  })
})

const updateOne = asyncHandler(async (req, res) => {
  log('PUT /serviceLists/:id', req.body)
  const ServiceList = req.model
  if(!req.params.id){
    return res.status(400).send({ error: 'Bad Request - missing ID'})
  }
  const list = await ServiceList.findById(req.params.id)
  cleanServices(list.set(req.body))
  const modifiedPaths = list.modifiedPaths()
  log('Modified:',modifiedPaths)
  if(modifiedPaths.length){
    setUpdated(list, req)
    await list.save()
  }
  return res.send(prepareModel(list))
})

const deleteOne = asyncHandler(async (req, res) => {
  console.log(`DELETE /serviceLists/:id`, req.params)
  const ServiceList = req.model
  const { id } = req.params
  await ServiceList.deleteOne({ _id: ObjectId(id) })
  res.send({ id })
})

const deleteMany = asyncHandler(async (req, res) => {
  console.log('POST /serviceLists/deleteMany', req.body)
  const ServiceList = req.model
  const { ids } = req.body
  if(!ids.length){
    return res.status(400).send({ error: 'There were no IDs submitted for deletion'})
  }
  await ServiceList.deleteMany({ _id: { $in: ids.map(ObjectId) }})
  res.send(ids)
})

router.use(setModel('ServiceList'))
router.route('/')
  .get(findAll)
  .post(express.json(), createOne)
router.post('/deleteMany', express.json(), deleteMany)
router.route('/:id')
  .get(findOne)
  .put(express.json(), updateOne)
  .delete(deleteOne)

module.exports = router