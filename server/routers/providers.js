const express = require('express')
const { prepare, prepareModel, cleanProvider, getQuery, log, getGeocoding, setUpdated } = require('../helpers/utils')
const { asyncHandler } = require('../helpers/errors')
const { getModel, setModel, ObjectId } = require('../helpers/models')
const router = express.Router()

const findAll = asyncHandler(async (req, res) => { // handle query params, with _page and _limit
  console.log('GET /providers', req.query)
  const Provider = req.model
  let q = getQuery(req.query, ['providerInfo.name','providerInfo.practiceName','providerInfo.specialty','providerInfo.photo','address'])
  const [ providers, count ] = await Promise.all([
    Provider.find(q.filter).sort(q.sort).skip(q.skip).limit(q.limit).lean(),
    Provider.countDocuments(q.filter)
  ])
  res.setHeader('X-Total-Count', count)
  res.send(providers.map(prepare))
})

const findOne = asyncHandler(async (req, res) => {
  console.log('GET /providers/:id', req.params, req.query)
  const Provider = req.model
  const provider = await Provider.findById(req.params.id).lean()
  if(!provider){
    return res.status(400).send({ error: 'There is no provider with that ID.'})
  }
  return res.send(prepare(provider))
})

const createOne = asyncHandler(async (req, res) => {
  console.log('POST /providers', req.body)
  const Provider = req.model
  const provider = cleanProvider(new Provider(req.body))
  if(provider.get('address')){
    const geocoding = await getGeocoding(provider.get('address'))
    if(geocoding){
      provider.set('address', geocoding.address)
      provider.set('location', {
        type: 'Point',
        coordinates: geocoding.coordinates
      })
    }
  }
  setUpdated(provider, req)
  return provider.save().then(saved => {
    return res.send(prepareModel(saved))
  })
})

const updateOne = asyncHandler(async (req, res) => {
  console.log('PUT /providers/:id')
  const Provider = req.model
  if(!req.params.id){
    return res.status(400).send({ error: 'Bad Request - missing ID'})
  }
  const provider = await Provider.findById(req.params.id)
  cleanProvider(provider.set(req.body))
  const modifiedPaths = provider.modifiedPaths()
  log('Modified:',modifiedPaths)
  if(!modifiedPaths.length){
    console.log('The provider has not been modified.')
    return res.send(prepareModel(provider))
  }
  const err = provider.validateSync(modifiedPaths)
  if(err){
    console.error('Update error', err)
    var messages = []
    for (field in err.errors) {
      messages.push(err.errors[field].message)
    }
    return res.status(400).send({ error: 'ValidationError: ' + messages.join('; ') })
  }
  
  if(modifiedPaths.includes('services')){
    const Service = getModel(req.cookies['st-env'], 'Service')
    // Load all services here to make sure they're valid IDs. (Project cost/isDefault for baseService calculation)
    const serviceIds = provider.get('services').map(s => s._id)
    const services = await Service.find({ _id: { $in: serviceIds } }).select('cost isDefault').lean()
    if(services.length < serviceIds.length){
      console.error('Error: one of the services is invalid... shouldn‘t be possible with current front-end')
      return res.status(400).send({ error: 'ValidationError: One of the services has an invalid ID' })
    }
    if(!provider.get('baseCost')){
      // if the provider doesn't already have a baseCost, check for an isDefault service to automatically set it...
      // this way we don't overwrite it if you've already manually set/changed it
      const baseService = services.find(s => s.isDefault)
      if(baseService){
        provider.set('baseCost', baseService.cost)
      }
    }
    // const providerPopulated = provider.populate({ path: 'services._id', model: 'Service' })
  }
  // if(modifiedPaths.includes('files')){
  //   const files = req.body.files
  //   const formerFiles = files.filter(p => !(p.rawFile instanceof File));
  //   const newFiles = files.filter(p => p.rawFile instanceof File);
  //   console.log('files', newFiles, formerFiles)
  //   if(newFiles.length){
  //     console.log('uploading files', newFiles)
  //     // uploadedFiles = [{ src, title }]
  //     // const uploadedFiles = await Promise.all(newFiles.map(uploadFile))
  //     // provider.set([ ...uploadedFiles, ...formerFiles ])
  //   }
  // }

  setUpdated(provider, req)
  await provider.save()
  return res.send(prepareModel(provider))
})

const deleteOne = asyncHandler(async (req, res) => {
  console.log(`DELETE /providers/:id`, req.params)
  const Provider = req.model
  const { id } = req.params
  await Provider.deleteOne({ _id: ObjectId(id) })
  res.send({ id })
})

const deleteMany = asyncHandler(async (req, res) => {
  console.log('POST /providers/deleteMany', req.body)
  const Provider = req.model
  const { ids } = req.body
  if(!ids.length){
    return res.status(400).send({ error: 'There were no IDs submitted for deletion'})
  }
  await Provider.deleteMany({ _id: { $in: ids.map(ObjectId) }})
  res.send(ids)
})

router.use(setModel('Provider'))

// TODO: updateMany?
router.route('/')
  .get(findAll)
  .post(express.json(), createOne)

router.post('/deleteMany', express.json(), deleteMany)

router.route('/:id')
  .get(findOne)
  .put(express.json(), updateOne)
  .delete(deleteOne)

module.exports = router