const util = require('util')
const rp = require('request-promise')
const ObjectId = require('mongoose').Types.ObjectId

const toNumber = num => {
  try {
    return Number(num)
  } catch(e){
    console.error('Couldnt parse number: ', num, e)
    return undefined
  }
}

const prepare = o => {
  const { _id, ...rest } = o
  return { id: _id, ...rest}
}

const prepareModel = m => prepare(m.toObject())

// move this from query into body. for simplicity
const getQuery = (params, searchFields = [], convertId = true) => {
  // true/false ?
  // filter the filters object by Model.fields?
  let { _skip, _limit, _sort, _order, ids_in, ids_nin, id, q, active, ...filter } = params
  if(id){
    filter._id = convertId ? ObjectId(id) : id
  }
  if(active){
    filter.active = active === 'true' ? true : { $ne: true }
  }
  if(ids_in){
    let ids = ids_in.split('|').filter(s => s.length)
    filter._id = { $in: convertId ? ids.map(ObjectId) : ids }
  }
  if(ids_nin){
    let ids = ids_nin.split('|').filter(s => s.length)
    filter._id = { $nin: convertId ? ids.map(ObjectId): ids }
  }
  if(q && searchFields.length){
    filter['$or'] = searchFields.map(field => ({ [field]: { $regex: q, $options: 'i' }}))
  }
  const sortField = !_sort || (_sort === 'id') ? '_id' : _sort
  const sortOrder = _order === 'DESC' ? -1 : 1
  return {
    skip: toNumber(_skip),
    limit: toNumber(_limit),
    sort: { [sortField]: sortOrder },
    filter
  }
}

const isArrayLength = arr => Array.isArray(arr) && arr.length
const cleanServices = model => { // remove override fields with value of null/undefined
  if(model.isModified('services')){
    model.set('services', model.services.map(service => {
      for (var prop in service) {
        if (service[prop] === null || service[prop] === undefined) {
          delete service[prop];
        }
        if(Array.isArray(service[prop]) && !service[prop].length){
          service[prop] = undefined
        }
      }
      return service
    }))
  }
  return model
}

const cleanProvider = model => {
  cleanServices(model)
  let validField = false
  console.log('clean provider')
  if(model.availabilities){
    console.log(model.availabilities)
    for (var day in model.availabilities) { 
      if(isArrayLength(model.availabilities[day])){
        validField = true
      }
    }
    if(!validField){
      console.log('no good availabilities. clear it!')
      model.availabilities = undefined
    }
  }
  return model
}

module.exports = { getQuery, toNumber, prepare, prepareModel,
  log: (...args) => args.map(o => {
    console.log(util.inspect(o, { depth: null, colors: true, maxArrayLength: null }))
  }), 
  getGeocoding: async address => {
    if(!address){
      console.error('Cannot geocode empty address', address)
      throw new Error('Cannot geocode empty address.')
    }
    const geolocationKey = process.env.GEOLOCATION_KEY
    const response = await rp({
      uri: `https://maps.googleapis.com/maps/api/geocode/json?key=${geolocationKey}&address=${address}`,
      json: true
    })
    if(response.status !== 'OK'){
      console.error('Geocoding error:', response)
      throw new Error('There was a Geocoding error.')
    }
    return {
      address: response.results[0].formatted_address.split(', USA')[0],
      coordinates: [
          response.results[0].geometry.location.lng,
          response.results[0].geometry.location.lat
      ]
    }
  },
  setUpdated: (model, req) => model.set('updatedBy', req.signedCookies['st-access']),
  isArrayLength,
  cleanServices,
  cleanProvider
}
