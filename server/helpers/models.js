const { mongoose, dbST, dbCC, dbApp } = require('../clients/mongoose')
const ObjectId = mongoose.Schema.Types.ObjectId;

const serviceRef = {
  _id: { type: ObjectId, ref: 'Service', required: true },
  // name: String,
  cost: Number,
  time: Number,
  includes: [String]
}

let schemas = {}

schemas['Service'] = mongoose.Schema({
  name: String,
  cost: Number,
  time: Number,
  includes: [String],
  isDefault: Boolean,
  category: String,
  adaCode: String,
  cptCode: String,
  description: String,
  updatedBy: String
}, {
  timestamps: true
})

schemas['ServiceList'] = mongoose.Schema({
  name: String,
  services: [serviceRef],
  updatedBy: String
}, {
  timestamps: true
})

// TODO: Use dates here.
const availabilitiesRef = [{ _id: false, from: Number, to: Number }]
schemas['Provider'] = mongoose.Schema({
  providerInfo: {
    name: { type: String, required: true },
    practiceName: String,
    degrees: String,
    specialty: String,
    secondarySpecialties: [String],
    rating: Number,
    email: String,
    photo: { type: String, required: true },
    additionalPhotos: [String],
    video: String,
    phoneNumber: String,
    website: String,
    notes: String,
    npi: String,
    description: String,
    languages: [String],
    education: [String]
  },
  address: { type: String },
  services:[serviceRef],
  serviceLists: [{ type: ObjectId, ref: 'ServiceList' }],
  availabilities: { 0: availabilitiesRef, 1: availabilitiesRef, 2: availabilitiesRef, 3: availabilitiesRef, 4: availabilitiesRef, 5: availabilitiesRef, 6: availabilitiesRef },
  files: [String],
  baseCost: Number,
  // BH;
  age: String,
  // gender: String,
  // sexualities: [String],
  identifiers: [String],
  insurances: [String],
  usStates: [String],
  issues: [String],
  modalities: [String],
  tags: [String],
  focuses: [String],
  experiences: [String],
  // Admin
  active: Boolean,
  updatedBy: String
}, {
  timestamps: true
})

schemas['User'] = mongoose.Schema({
  _id: String,
  firstName: String,
  lastName: String,
  email: String,
  invitedBy: String,
  dob: String,
  phone: String,
  updatedBy: String
  // todo: all the extra stuff that a user inputs
}, {
  timestamps: true
})

schemas['Appointment'] = mongoose.Schema({
  details: {
    times: [{
      date: String,
      time: String
    }],
    confirmedTime: Date
  },
  providerId: { type: ObjectId, ref: 'Provider' },
  patientId: { type: String, ref: 'User' },
  payment: {
    token: String,
    last4: String
  },
  stripeCustomer: String,
  services: [{
    _id: { type: ObjectId, ref: 'Service' }, // not required yet, like the other serviceRefs
    name: String,
    cost: Number,
    time: Number
  }],
  totalCost: Number,
  status: {
    type: String,
    enum: ['booked', 'checkedIn', 'confirmed']
  },
  videoLink: String,
  checkedInAt: Date,
  updatedBy: String
}, {
  timestamps: true
})

schemas['Action'] = mongoose.Schema({
  userId: { type: String, ref: 'User' },
  action: String,
  detail: String,
  link: String
}, {
  timestamps: true
})

const getCollectionName = (env, name) => {
  if(name === 'ServiceList') return 'serviceLists'
  if(env === 'dev'){
    switch(name){
      case 'User': return 'usersDev'
      case 'Appointment': return 'appointmentsDev'
      case 'Action': return 'actionsDev'
      default:
    }
  }
  return undefined
}

const getDB = env => {
  switch(env){
    case 'claude': return dbCC;
    case 'app': return dbApp;
    default: return dbST;
  }
}

const getModel = (env, name) => {
  const collection = getCollectionName(env, name)
  return getDB(env).model(name, schemas[name], collection)
}

const setModel = name => (req, res, next) => {
  req.model = getModel(req.cookies['st-env'], name)
  next()
}

module.exports = { getModel, setModel, ObjectId: mongoose.Types.ObjectId }
