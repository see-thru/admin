// const uri = "mongodb+srv://console-user:mk1es79CNkEJaNuI@cluster0-d2uhy.gcp.mongodb.net/test?retryWrites=true";
// common/psych -> currently psych
const uri = process.env.MONGO_URI
const uriClaude = process.env.MONGO_URI_CLAUDE
const uriApp = process.env.MONGO_URI_APP
const mongoose = require('mongoose')

mongoose.Promise = global.Promise;

// Connecting to the database
mongoose.connect(uri, { useNewUrlParser: true }).then(() => {
  console.log('Successfully connected to the database');
}).catch(err => {
  console.log('Could not connect to the database. Exiting now...', err);
  process.exit();
});

const dbCC = mongoose.createConnection(uriClaude, { useNewUrlParser: true })
const dbApp = mongoose.createConnection(uriApp, { useNewUrlParser: true })

module.exports = { 
  mongoose, 
  dbST: mongoose.connection,
  dbCC,
  dbApp
}
