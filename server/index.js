const express = require('express')
const cookieParser = require('cookie-parser')
const validEmails = require('./helpers/users')
const secretKey = process.env.ADMIN_SECRET_KEY
const cookieParams = { signed: true, maxAge: 1209600000 } // 14 days
const CLIENT_ID = process.env.ADMIN_CLIENT_ID

const app = express.Router().use(cookieParser(secretKey))

const verifyToken = (req, res, next) => {
  if(req.signedCookies['st-access']){
    next()
  } else {
    res.status(403).send({ error: 'You do not have permission to view this data' }) // this should automatically log user out of admin app
  }
}
 
app.post('/login', express.json(), (req,res) => {
  if(req.body.id_token){
    const { OAuth2Client } = require('google-auth-library');
    const client = new OAuth2Client(CLIENT_ID)
    async function verify() {
      const ticket = await client.verifyIdToken({
          idToken: req.body.id_token,
          audience: CLIENT_ID
      })
      const payload = ticket.getPayload()
      if(!validEmails.includes(payload['email'].toLowerCase())){
        return res.status(403).send({ error: 'You need a seethru.healthcare email to sign up.'})
      }
      return res.cookie('st-access', payload['email'], cookieParams).send('OK')
    }
    verify().catch(e => {
      console.error(e)
      res.status(403).send({ error: 'That is not a valid ID Token'})
    });
  } else {
    return res.status(403).send({ error: 'That is not a valid ID Token'})
  }
})

const appRouter = express.Router().use(verifyToken)
appRouter.use('/providers', require('./routers/providers'))
appRouter.use('/services', require('./routers/services'))
appRouter.use('/serviceLists', require('./routers/serviceLists'))
appRouter.use('/users', require('./routers/users'))
appRouter.use('/appointments', require('./routers/appointments'))
appRouter.use('/actions', require('./routers/actions'))
app.use(appRouter)

const server = express().use('/api', app) // mount path
const port = process.env.PORT || 5000
server.listen(port)
console.log('App is listening on port ' + port)
