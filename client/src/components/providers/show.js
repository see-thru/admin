import React, { Component, Fragment } from 'react'
import styled from 'styled-components'
import { Show, TabbedShowLayout, Tab, SimpleShowLayout, TextField, NumberField, EmailField, Labeled,
  DateField, BooleanField, UrlField, ReferenceArrayField, SingleFieldList, ChipField, EditButton, SelectField
} from 'react-admin'
import { AvailabilitiesField, StringsField, ServicesField, OptionalField, OptionalTextField, StringsCondensedField, UrlsField, CurrencyField } from '../fields'
import PageTitle from './page-title'
import CardActions from '@material-ui/core/CardActions';
import Typography from '@material-ui/core/Typography'

import ActivateButton from './ActivateButton'
import { genderChoices } from '../../utils/options'
import { isArrayLength } from '../../utils/helpers';

const ProviderShowActions = ({ basePath, data, resource }) => (
  <CardActions style={{ zIndex: 2, display: 'inline-block', float: 'right' }}>
    <EditButton basePath={basePath} record={data} />
    <ActivateButton basePath={basePath} record={data} resource={resource} />
  </CardActions>
);

const StyledShowLayout = styled(SimpleShowLayout)`
  padding: 0 !important;
`
const StyledProviderPhoto = styled.div`  
  border: 1px solid #eee;
  display: inline-block;
  position: relative;
  > img {
    opacity: 0.3;
    display: block;
    max-height: 200px;
  }
  .photo-focus {
    position: absolute;
    overflow: hidden;
    top: 50%;
    left: 50%;
    border-radius: 50%;
    transform: translate(-50%, -50%);
    img {
      object-fit: cover;
      object-position: center;
      height: 100%;
      width: 100%;
    }
  }
`
class ProviderPhoto extends Component {
  state = { dimension: 0 }
  onImgLoad = ({ target: { offsetHeight, offsetWidth } }) => {
    if(offsetHeight && offsetWidth){
      this.setState({ dimension: Math.min(offsetHeight, offsetWidth )})
    }
  }
  render(){
    const { src } = this.props
    const { dimension } = this.state
    return (
      <StyledProviderPhoto>
        <img src={src} onLoad={this.onImgLoad} alt="Provider" />
        <div className="photo-focus" style={{width: dimension, height: dimension}}>
          <img src={src} alt="Provider" />
        </div>
      </StyledProviderPhoto>
    )
  }
}

const ShowProviderInfo = ({ record: { providerInfo } }) => (
  <StyledShowLayout record={providerInfo}>
    <TextField source="name" />
    { providerInfo.practiceName && <TextField source="practiceName" />}
    { providerInfo.degrees && <TextField source="degrees" />}
    <NumberField source="rating" />
    <UrlField source="photo" target="_blank" rel="noopener noreferrer" />
    <ProviderPhoto src={providerInfo.photo} />
    { isArrayLength(providerInfo.additionalPhotos) && 
      <UrlsField source="additionalPhotos" label="Additional Photos" />
    }
    { providerInfo.video && <UrlField source="video" /> }
    { providerInfo.email && <EmailField source="email" />}
    { providerInfo.phoneNumber && <TextField source="phoneNumber" />}
    { providerInfo.website && <UrlField source="website" target="_blank" rel="noopener noreferrer" />}
    { providerInfo.npi && <TextField source="npi" />}
    { isArrayLength(providerInfo.education) &&
      <StringsField source="education" label="Education" />
    }
    { isArrayLength(providerInfo.languages) &&
      <StringsField source="languages" label="Languages" />
    }
    { isArrayLength(providerInfo.secondarySpecialties) &&
      <StringsField source="secondarySpecialties" label="Secondary Specialties" />
    }
  </StyledShowLayout>
)

const ProviderLink = ({ record }) => {
  const domain = window.stEnv === 'claude' ? 'https://app.claude.care' : record.active ? 'https://bh.seethru.healthcare' : 'https://seethru-dev.firebaseapp.com'
  const link = `${domain}/provider/${record.id}`
  return (
    <Typography component="span" body1="body1">
      <a href={link} target="_blank" rel="noopener noreferrer">{link}</a>
    </Typography>
  )
}

const LocationField = ({ record }) => {
  if(record.location && record.location.coordinates){
    const [ lng, lat ] = record.location.coordinates
    return (
      <Typography component="span" body1="body1">
        <a href={`http://www.google.com/maps/place/${lat},${lng}`} target="_blank" rel="noopener noreferrer">
          { lat },{ lng }
        </a>
      </Typography>
    )
  } else return null
}
LocationField.defaultProps = { addLabel: true }

const FilesField = ({ record }) => {
  return record.files && record.files.length ? (
    <Labeled label="Related files">
      <Fragment>
        { record.files.map((file, index) => (
          <Typography component="span" body1="body1" key={index}>
            <a href={file} target="_blank" rel="noreferrer noopener">{ file }</a>
          </Typography>
        ))}
      </Fragment>
    </Labeled>
  ) : null
}

export default props => (
  <Show {...props} title={<PageTitle />} actions={<ProviderShowActions />}>
    <TabbedShowLayout>
      <Tab label="Provider Info">
        <TextField source="id" label="Provider ID" />
        <ProviderLink />
        <ShowProviderInfo />
      </Tab>
      <Tab label="BH">
        <StringsCondensedField source="usStates" label="US States" />
        <TextField source="age" />
        <TextField source="providerInfo.specialty" label="Specialty" />
        <StringsCondensedField source="issues" />
        <StringsCondensedField source="identifiers" />
        <StringsCondensedField source="modalities" />
        <StringsCondensedField source="insurances" />
        <StringsCondensedField source="focuses" />
        <StringsCondensedField source="experiences" />
        <StringsCondensedField source="tags" />
      </Tab>
      <Tab label="Services">
        <OptionalField source="baseCost" label="Base Service Cost">
          <CurrencyField />
        </OptionalField>
        <ServicesField source="services" />
        <ReferenceArrayField label="Service Lists" reference="serviceLists" source="serviceLists">
          <SingleFieldList linkType="show">
            <ChipField source="name" />
          </SingleFieldList>
        </ReferenceArrayField>
      </Tab>
      <Tab label="Calendar">
        <AvailabilitiesField />
      </Tab>
      <Tab label="Admin">
        <FilesField />
        <DateField source="createdAt" showTime />
        <OptionalField source="updatedAt" label="Updated at"><DateField showTime /></OptionalField>
        <OptionalTextField source="updatedBy" label="Updated by" />
        <BooleanField source="active" />
      </Tab>
    </TabbedShowLayout>
  </Show>
)