export default values => {
  const errors = { providerInfo: {} };
  if(!values.providerInfo){
    errors.providerInfo.name = ['Provider Name is required'];
    return errors;
  }
  
  if (!values.providerInfo.name) {
    errors.providerInfo.name = ['Provider Name is required'];
  }
  // if (!values.providerInfo.photo) {
  //   errors.providerInfo.photo = ['Photo is required']; // todo: validate url
  // }
  // if(!values.address){
  //   errors.address = ['Address is required'];
  // }
  return errors
}