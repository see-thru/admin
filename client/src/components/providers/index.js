import list from './list'
import edit from './edit'
import show from './show'
import create from './create'

export default { list, edit, show, create }