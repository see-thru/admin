import React from 'react'

export default ({ record }) => {
  return <span>Provider{record ? `: ${record.providerInfo.name}` : ''}</span>;
};