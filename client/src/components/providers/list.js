import React, { Fragment } from 'react'
import { List, Datagrid, TextField, DateField, Filter, Responsive, SimpleList,
  TextInput, SelectInput, BooleanField, NullableBooleanInput, BulkDeleteButton,
  downloadCSV
} from 'react-admin'
import { unparse as convertToCSV } from 'papaparse/papaparse.min';
import ActivateManyButton from './ActivateManyButton'
import DeactivateManyButton from './DeactivateManyButton'

const ProviderFilter = (props) => (
  <Filter {...props}>
    <TextInput label="Search" source="q" alwaysOn />
    <SelectInput label="Updated By" source="updatedBy" allowEmpty choices={[
      { id: 'Mark@seethru.healthcare', name: 'Mark' },
      { id: 'adi@seethru.healthcare', name: 'Adi' },
      { id: 'jessica@seethru.healthcare', name: 'Jess' },
      { id: 'nate@seethru.healthcare', name: 'Nate' },
      { id: 'ariel@seethru.healthcare', name: 'Nate' }
    ]}/>
    <NullableBooleanInput label="Active" source="active" allowEmpty />
  </Filter>
);

const ProviderActionButtons = props => (
  <Fragment>
    <ActivateManyButton {...props} />
    <DeactivateManyButton {...props} />
    <BulkDeleteButton {...props} />
  </Fragment>
)

const exporter = providers => {
  const providersForExport = providers.map(provider => {
    const { 
      providerInfo: { secondarySpecialties, languages, education, ...info }, 
      location, services, serviceLists, files, ...rest } = provider;
    return {
      ...info,
      ...rest,
      secondarySpecialties: secondarySpecialties ? secondarySpecialties.join(',') : '',
      languages: languages ? languages.join(',') : '',
      education: education ? education.join(',') : '',
      coordinates: location ? `${location.coordinates[1]},${location.coordinates[0]}` : '',
      files: files ? files.join(',') : ''
    }
  });
  const csv = convertToCSV({
      data: providersForExport,
      fields: ['id', 'active', 'name', 'degrees', 'photo', 'practiceName', 'address', 'coordinates', 'specialty', 'secondarySpecialties', 'rating', 'email', 'phoneNumber', 'website', 'notes', 'npi', 'description', 'languages', 'education', 'files', 'createdAt', 'updatedAt', 'updatedBy']
  });
  downloadCSV(csv, 'providers'); // download as 'posts.csv` file
}

export default props => (
  <List {...props} 
    filters={<ProviderFilter />} 
    perPage={50} 
    bulkActionButtons={<ProviderActionButtons />} 
    title="All Providers"
    exporter={exporter}>
    <Responsive
      small={
        <SimpleList linkType="show" primaryText={record => record.providerInfo.name} />
      }
      medium={
        <Datagrid rowClick="show">
          <TextField source="providerInfo.name" label="Name" />
          <TextField source="providerInfo.practiceName" label="Practice name" />
          <TextField source="providerInfo.specialty" label="Specialty" />
          <TextField source="updatedBy" />
          <DateField source="updatedAt" />
          <BooleanField source="active" />
        </Datagrid>
      }
    />
  </List>
);

