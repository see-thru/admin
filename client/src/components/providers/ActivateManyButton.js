import React from 'react';
import { connect } from 'react-redux';
import { Button, crudUpdateMany } from 'react-admin';
import ActivateIcon from '@material-ui/icons/VerifiedUser';

const ActivateManyButton = ({ basePath, crudUpdateMany, resource, selectedIds }) => {
	const handleClick = () => crudUpdateMany(resource, selectedIds, { active: true }, basePath);
	return (
		<Button label="Activate" onClick={handleClick}><ActivateIcon /></Button>
	)
}

export default connect(undefined, { crudUpdateMany })(ActivateManyButton)