import React from 'react';
import { connect } from 'react-redux';
import { Button, crudUpdateMany } from 'react-admin';
import { compose } from 'recompose';
import { withStyles } from '@material-ui/core';
import DeactivateIcon from '@material-ui/icons/IndeterminateCheckBox';
import { fade } from '@material-ui/core/styles/colorManipulator';

const styles = theme => ({
	redButton: {
			color: theme.palette.error.main,
			'&:hover': {
					backgroundColor: fade(theme.palette.error.main, 0.12),
					// Reset on mouse devices
					'@media (hover: none)': {
							backgroundColor: 'transparent',
					},
			},
	},
});

const DeactivateManyButton = ({ classes, basePath, crudUpdateMany, resource, selectedIds }) => {
	const handleClick = () => crudUpdateMany(resource, selectedIds, { active: false }, basePath);
	return (
		<Button className={classes.redButton} 
			label="Deactivate" 
			onClick={handleClick}><DeactivateIcon /></Button>
	)
}

export default compose(
	connect(undefined, { crudUpdateMany }),
	withStyles(styles)
)(DeactivateManyButton)