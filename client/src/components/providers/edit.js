import React from 'react'
import { LongTextInput, FileInput, FileField, NumberInput, Edit, TabbedForm, DisabledInput, FormTab,
  TextInput, BooleanInput, ReferenceArrayInput, SelectInput, SelectArrayInput, AutocompleteArrayInput, CheckboxGroupInput
} from 'react-admin'
import Typography from '@material-ui/core/Typography'
import { AvailabilitiesInput, ServicesInput, StringsInput, StringsCondensedInput } from '../fields'
import PageTitle from './page-title'
import validateProvider from './validate'
import { insuranceChoices, stateChoices, issuesChoices, specialtyChoices, focusesChoices, typesChoices, ageChoices, identifierChoices, experienceChoices } from '../../utils/options';
import { alphabetize } from '../../utils/helpers';

export default props => (
  <Edit {...props} title={<PageTitle />}>
    <TabbedForm submitOnEnter={false} validate={validateProvider} redirect="show">
      <FormTab label="Provider Info">
        <DisabledInput source="id" label="Provider ID" />
        <TextInput source="providerInfo.name" label="Name" />
        <TextInput source="providerInfo.practiceName" label="Practice Name" />
        <TextInput source="providerInfo.email" label="Email" />
        <TextInput source="providerInfo.phoneNumber" label="Phone Number" />
        <TextInput source="providerInfo.photo" label="Photo" />
        <StringsInput source="providerInfo.additionalPhotos" label="Additional Photos" />
        <TextInput source="providerInfo.video" label="Video" />
        <TextInput source="providerInfo.degrees" label="Degrees" />
        <NumberInput source="providerInfo.rating" label="Rating" />
        <StringsInput source="providerInfo.education" label="Education" />
        <StringsInput source="providerInfo.languages" label="Languages" />
        <StringsInput source="providerInfo.secondarySpecialties" label="Secondary Specialties" />
        <LongTextInput source="providerInfo.description" label="Description" />
        <TextInput source="providerInfo.website" label="Website" />
        <TextInput source="providerInfo.npi" label="NPI" />
      </FormTab>
      <FormTab label="BH">
        <AutocompleteArrayInput source="usStates" choices={stateChoices} label="US States" parse={alphabetize} />
        <SelectInput source="age" choices={ageChoices} />
        <SelectInput source="providerInfo.specialty" choices={specialtyChoices} label="Specialty" options={{helperText: 'Subtext below provider’s name'}} />
        <AutocompleteArrayInput source="issues" choices={issuesChoices} options={{ fullWidth: true }} parse={alphabetize} />
        <AutocompleteArrayInput source="identifiers" choices={identifierChoices} options={{ fullWidth: true }} parse={alphabetize} />
        <AutocompleteArrayInput source="modalities" choices={typesChoices} options={{ fullWidth: true}} parse={alphabetize} />

        <AutocompleteArrayInput source="insurances" choices={insuranceChoices} options={{ fullWidth: true }} parse={alphabetize} />
        <StringsCondensedInput source="tags" label="Tags" />
        <><Typography variant="caption">Used for search algorithm. Never displayed.</Typography></>
        
        <AutocompleteArrayInput source="focuses" choices={focusesChoices} options={{ fullWidth: true }} parse={alphabetize} />
        <AutocompleteArrayInput source="experiences" label="Personal Experiences" choices={experienceChoices} options={{ fullWidth: true }} parse={alphabetize} />
      </FormTab>
      <FormTab label="Services">
        <NumberInput source="baseCost" label="Base Service Cost" options={{helperText:"Shown in search results (blue box)"}} />
        <ServicesInput />
        <ReferenceArrayInput source="serviceLists" reference="serviceLists" label="Service Lists" fullWidth>
          <SelectArrayInput optionText="name" />
        </ReferenceArrayInput>
      </FormTab>
      <FormTab label="Calendar">
        <AvailabilitiesInput />
      </FormTab>
      <FormTab label="Admin">
        <StringsInput source="files" label="Related files (upload via Cloudinary)" />
        { false && 
          <FileInput source="files" label="Related files" accept="application/pdf" multiple>
            <FileField source="src" title="title" />
          </FileInput>
        }
        <BooleanInput source="active" />
      </FormTab>
    </TabbedForm>
  </Edit>
);