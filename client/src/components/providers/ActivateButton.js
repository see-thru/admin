import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, crudUpdate } from 'react-admin';
import ActivateIcon from '@material-ui/icons/VerifiedUser';

class ActivateButton extends Component {
	handleClick = () => {
		const { crudUpdate, resource, record, basePath } = this.props;
		console.log('ActivateButton: ', this.props)
		crudUpdate(resource, record.id, { active: true }, record, basePath);
	};

	render() {
		const { record } = this.props
		return record && !record.active ? (
			<Button label="Activate" onClick={this.handleClick}><ActivateIcon /></Button>
		) : null;
	}
}

export default connect(undefined, { crudUpdate })(ActivateButton)
