import React from 'react'
import { Layout, AppBar } from 'react-admin'
import EnvSwitch from './env-switch'

const MyAppBar = props => <AppBar {...props} userMenu={<EnvSwitch />} />;

const MyLayout = (props) => <Layout {...props} appBar={MyAppBar} />;

export default MyLayout;