import React, { Component } from 'react'
import GoogleLogin from 'react-google-login'
import styled from 'styled-components'

const StyledContainer = styled.main`
  background: #d3d8e8;
  display: flex;
  justify-content: center;
  align-items: center;
  text-align: center;
  > div {
    padding: 30px 20px;
    width: 100%;
    max-width: 450px;
    border-radius: 5px;
    background: rgba(255,255,255,.5);
    box-shadow: 0px 0px 5px rgba(0,0,0,.5);
  }
  h1 {
    font-size: 2rem;
    margin-bottom: 1rem;
  }
  .alert {
    cursor: pointer;
    background: rgba(220, 83, 83, 0.5);
    padding: 20px 10px;
    border-radius: 5px;
    margin: 20px 0;
    color: #333;
  }
`

class Login extends Component {
  constructor(){
    super()
    this.state = {}
  }

  onFailure = e => {
    console.error(e)
    let error = e.error === 'popup_closed_by_user' ? 'You did not complete the sign in.' : 'There was an error with your sign in.'
    this.setState({ error })
  }

  onSuccess = async response => {
    try {
      await fetch('/api/login', {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ id_token: response.tokenId })
      })
      window.location.reload()
    } catch(e){
      console.error(e)
      this.setState({ error: 'There was an error with your ID token. Please try again or contact the SeeThru dev team.' })
    }
  }

  clearError = () => {
    this.setState({ error: undefined })
  }
  
  render(){
    const { error } = this.state
    return (
      <StyledContainer>
        <div>
          <h1>SeeThru Console</h1>
          { error && <div className="alert" onClick={this.clearError}>
            { error }<br />Please try again or contact the SeeThru dev team.
          </div>}
          <GoogleLogin
            clientId='632999292766-pt0dmm4u1hmpfpo5pjjuihqsbs60f7f8.apps.googleusercontent.com'
            onFailure={this.onFailure}
            onSuccess={this.onSuccess}
            redirectUri={window.location.origin}  />
        </div>
      </StyledContainer>
    )
  }
}

export default Login