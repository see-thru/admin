import React from 'react'
import { Create, Edit, SimpleForm, DisabledInput, ArrayInput, SimpleFormIterator,
  TextInput, NumberInput, SelectInput
} from 'react-admin'
import { DateTimeUTCInput, DateUTCInput } from './UTCInputs'
import { statusChoices } from '.'

export const create = props => (
  <Create {...props} title="Create Appointment">
    <SimpleForm redirect="show">
      <DateTimeUTCInput source="details.confirmedTime" label="Confirmed Time" />
      <ArrayInput source="details.times" label="Requested Times">
        <SimpleFormIterator>
          <DateUTCInput source="date" label="Date" />
          <TextInput source="time" label="Time" />
        </SimpleFormIterator>
      </ArrayInput>
      <ArrayInput source="services">
        <SimpleFormIterator>
          <TextInput source="name" label="Name" />
          <NumberInput source="cost" label="Cost" />
          <NumberInput source="time" label="Time" />
        </SimpleFormIterator>
      </ArrayInput>
      <SelectInput source="status" choices={statusChoices} />
      <TextInput source="videoLink" label="Private video link" />
    </SimpleForm>
  </Create>
)

export const edit = props => (
  <Edit {...props} title="Edit Appointment">
    <SimpleForm redirect="show">
      <DisabledInput source="id" label="Appointment ID" />
      <DateTimeUTCInput source="details.confirmedTime" label="Confirmed Time" />
      <ArrayInput source="details.times" label="Requested Times">
        <SimpleFormIterator>
          <DateUTCInput source="date" label="Date" />
          <TextInput source="time" label="Time" />
        </SimpleFormIterator>
      </ArrayInput>
      <ArrayInput source="services">
        <SimpleFormIterator>
          <TextInput source="name" label="Name" />
          <NumberInput source="cost" label="Cost" />
          <NumberInput source="time" label="Time" />
        </SimpleFormIterator>
      </ArrayInput>
      <SelectInput source="status" choices={statusChoices} />
      <TextInput source="videoLink" label="Private video link" />
    </SimpleForm>
  </Edit>
)