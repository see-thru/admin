import React from 'react'
import { List, Datagrid, TextField, ReferenceField, EditButton,
  SelectInput, Filter, ReferenceInput, AutocompleteInput,
} from 'react-admin'
import { CurrencyField, FullNameField } from '../fields'
import { statusChoices } from '.'


const AppointmentFilter = (props) => (
  <Filter {...props}>
    <ReferenceInput label="By Patient" source="patientId" reference="users">
      <AutocompleteInput optionText="email" inputValueMatcher={() => null} />
    </ReferenceInput>
    <ReferenceInput label="By Provider" source="providerId" reference="providers">
      <AutocompleteInput optionText="providerInfo.name" inputValueMatcher={() => null} />
    </ReferenceInput>
    <SelectInput label="Status" source="status" allowEmpty choices={statusChoices}/>
  </Filter>
);

export default props => (
  <List {...props} filters={<AppointmentFilter />} perPage={25} title="All Appointments">
    <Datagrid rowClick="show">
      <ReferenceField label="Patient" source="patientId" reference="users" linkType="show">
        <FullNameField />
      </ReferenceField>
      <ReferenceField label="Provider" source="providerId" reference="providers" linkType="show">
        <TextField source="providerInfo.name" />
      </ReferenceField>
      <CurrencyField source="totalCost" />
      <EditButton onClick={e => e.stopPropagation()} />
    </Datagrid>
  </List>
)