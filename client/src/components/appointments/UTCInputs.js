import React from 'react'
import { DateInput, DateTimeInput } from 'react-admin'

// https://github.com/marmelab/react-admin/blob/master/packages/ra-ui-materialui/src/input/DateTimeInput.js
const leftPad = (nb = 2) => value => ('0'.repeat(nb) + value).slice(-nb);
const leftPad4 = leftPad(4);
const leftPad2 = leftPad(2);

const convertDateToString = v => {
  if (!(v instanceof Date) || isNaN(v)) return '';
  const yyyy = leftPad4(v.getFullYear());
  const MM = leftPad2(v.getMonth() + 1);
  const dd = leftPad2(v.getDate());
  const hh = leftPad2(v.getHours());
  const mm = leftPad2(v.getMinutes());
  return `${yyyy}-${MM}-${dd}T${hh}:${mm}`;
};

export const toUTCDateString = v => {
  const date = new Date(v);
  return `${date.getUTCMonth() + 1}/${date.getUTCDate()}/${date.getUTCFullYear()}`
  // const yyyy = leftPad4(date.getUTCFullYear());
  // const MM = leftPad2(date.getUTCMonth() + 1);
  // const dd = leftPad2(date.getUTCDate());
  // return `${yyyy}-${MM}-${dd}`;
}

// We want to save the time as an objective time. Where it doesn't matter what timezone you're looking from. 

// For display, add offset so you see the UTC time in your own timezone, for editing
const dateFormatter = v => {
  let parsedDate = new Date(v);
  let adjustedDate = new Date(parsedDate.getTime() + parsedDate.getTimezoneOffset() * 60000);
  // console.log(`retrieving (format): ${v} -> ${adjustedDate.toISOString()}`)
  return convertDateToString(adjustedDate);
};

// For saving, remove your timezone offset so that we can save the selected time in UTC
const dateParser = v => {
  let parsedDate = new Date(v);
  let adjustedDate = new Date(parsedDate.getTime() - parsedDate.getTimezoneOffset() * 60000);
  // console.log(`saving (parser): ${v} -> ${adjustedDate.toISOString()}`)
  return adjustedDate;
};

export const DateTimeUTCInput = props => (
  <DateTimeInput { ...props } format={dateFormatter} parse={dateParser} />
)

export const DateUTCInput = props => (
  <DateInput { ...props } format={dateFormatter} parse={dateParser} />
)