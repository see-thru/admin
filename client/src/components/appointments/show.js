import React from 'react'
import { Datagrid, TextField, ReferenceField, NumberField, DateField, 
  Show, SimpleShowLayout, ArrayField, SelectField
} from 'react-admin'
import { CurrencyField, FullNameField, OptionalTextField, OptionalField } from '../fields'
import { Typography } from '@material-ui/core'
import { statusChoices } from '.'
import { toUTCDateString } from './UTCInputs'

const RequestedTimes = ({ record }) => (
  <ul style={{margin: '0'}}>
    { record.details.times && record.details.times.map((t, index) => (
      <li key={index}><Typography>{ toUTCDateString(t.date) } - { t.time }</Typography></li>
    ))}
  </ul>
)
RequestedTimes.defaultProps = { addLabel: true }

const StripeLink = ({ record }) => (
  <Typography component="span" body1="body1">
    <a href={`https://dashboard.stripe.com/${window.stEnv==='dev'?'test/':''}customers/${record.stripeCustomer}`}
      target="_blank" rel="noopener noreferrer">{ record.stripeCustomer }</a>
  </Typography>
)
StripeLink.defaultProps = { addLabel: true }

export default props => (
  <Show {...props} title="View Appointment">
    <SimpleShowLayout>
      <TextField source="id" label="Appointment ID" />
      <ReferenceField label="Patient" source="patientId" reference="users" linkType="show">
        <FullNameField />
      </ReferenceField>
      <ReferenceField label="Provider" source="providerId" reference="providers" linkType="show">
        <TextField source="providerInfo.name" />
      </ReferenceField>
      <StripeLink source="stripeCustomer" />
      <CurrencyField source="totalCost" />
      <DateField source="details.confirmedTime" showTime label="Confirmed Time" options={{ timeZone: 'UTC' }} />
      <RequestedTimes label="Requested Times" />
      <ArrayField source="services">
        <Datagrid>
          <TextField source="name" />
          <CurrencyField source="cost" />
          <NumberField source="time" />
        </Datagrid>
      </ArrayField>
      <SelectField source="status" choices={statusChoices} />
      <OptionalTextField source="videoLink" label="Video link" />
      <OptionalField source="checkedInAt" label="Checked in at"><DateField showTime /></OptionalField>
      <DateField source="createdAt" showTime />
      <OptionalField source="updatedAt" label="Updated at"><DateField showTime /></OptionalField>
      <OptionalTextField source="updatedBy" label="Updated by" />
    </SimpleShowLayout>
  </Show>
)