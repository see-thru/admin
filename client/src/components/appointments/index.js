import { create, edit } from './edit'
import list from './list'
import show from './show'

export const statusChoices = [
  { id: 'booked', name: 'Booked' },
  { id: 'checkedIn', name: 'Checked In' },
  { id: 'confirmed', name: 'Confirmed' }
]

export default { list, show, edit, create }