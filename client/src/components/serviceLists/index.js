import React from 'react'
import { List, Datagrid, TextField, DisabledInput, DateField,
  Edit, Create, SimpleForm, TextInput, Show, SimpleShowLayout, EditButton, CloneButton
} from 'react-admin'
import { ServicesInput, ServicesField, OptionalField, OptionalTextField } from '../fields'
import CardActions from '@material-ui/core/CardActions';

const list = props => (
  <List {...props} perPage={50} title="All Service Lists">
    <Datagrid rowClick="show">
      <TextField source="name" />
    </Datagrid>
  </List>
);

const PageTitle = ({ record }) => (
  <span>Service List{record ? `: ${record.name}` : ''}</span>
)

const ServiceListShowActions = ({ basePath, data }) => (
  <CardActions style={{ zIndex: 2, display: 'inline-block', float: 'right' }}>
    <EditButton basePath={basePath} record={data} />
    <CloneButton basePath={basePath} record={data} />
  </CardActions>
);

const show = props => (
  <Show { ...props } title={<PageTitle />} actions={<ServiceListShowActions />}>
    <SimpleShowLayout>
      <TextField source="id" label="Service List ID" />
      <TextField source="name" />
      <ServicesField source="services" />
      <DateField source="createdAt" showTime />
      <OptionalField source="updatedAt" label="Updated at"><DateField showTime /></OptionalField>
      <OptionalTextField source="updatedBy" label="Updated by" />
    </SimpleShowLayout>
  </Show>
)

const edit = props => (
  <Edit {...props} title={<PageTitle />}>
    <SimpleForm redirect="show">
      <DisabledInput source="id" label="Service List ID" />
      <TextInput source="name" />
      <ServicesInput />
    </SimpleForm>
  </Edit>
);

const create = props => (
  <Create {...props} title="Create a Service List">
    <SimpleForm redirect="show">
      <TextInput source="name" />
      <ServicesInput />
    </SimpleForm>
  </Create>
);

export default { list, show, edit, create }