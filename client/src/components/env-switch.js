import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Select from '@material-ui/core/Select'
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import { withStyles } from '@material-ui/core/styles';
import Cookies from 'js-cookie'

window.stEnv = Cookies.get('st-env')
if(window.location.hash && window.location.hash.length){
  const newEnv = window.location.hash.slice(1)
  if(window.stEnv !== newEnv){
    Cookies.set('st-env', newEnv)
    window.location.hash = ''
    window.stEnv = newEnv
  }
}

const styles = theme => ({
  root: {
    borderLeft: '1px solid #aaa',
    paddingLeft: '20px',
    marginLeft: '8px'
  },
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 120,
  },
  inputLabel: {
    color: '#d2ccff'
  },
  select: {
    color: '#fff'
  }
});

class EnvSwitch extends Component {
  state = { 
    stEnv: window.stEnv
  }
  static propTypes = {
    classes: PropTypes.object.isRequired
  }

  handleChange = event => {
    const stEnv = event.target.value
    this.setState({ stEnv })
    if(stEnv === window.stEnv) return
    Cookies.set('st-env', stEnv)
    window.location.reload()
  }

  render() {
    const { stEnv } = this.state
    const { classes } = this.props
    return (
      <form className={classes.root} autoComplete="off">
        <FormControl className={classes.formControl}>
          <InputLabel htmlFor="st-env" className={classes.inputLabel}>Environment</InputLabel>
          <Select
            value={stEnv || ''}
            onChange={this.handleChange}
            inputProps={{
              name: 'st-env',
              id: 'st-env',
            }}
            displayEmpty
            className={classes.select}
          >
            <MenuItem value="psych">Psych</MenuItem>
            <MenuItem value="dev">Dev</MenuItem>
            {/* <MenuItem value="app">App</MenuItem>
            <MenuItem value="claude">Claude</MenuItem> */}
          </Select>
        </FormControl>
      </form>
    )
  }
}

export default withStyles(styles)(EnvSwitch)