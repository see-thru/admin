import React from 'react'
import get from 'lodash/get'
import { List, Datagrid, TextField, Show, SimpleShowLayout, 
  ReferenceField, DateField, UrlField, Filter, TextInput, SelectInput,
  ReferenceInput, AutocompleteInput
} from 'react-admin'
import { FullNameField, OptionalField } from '../fields'

const OptionalUserField = ({ children, ...props }) => {
  const value = get(props.record, 'userId')
  return value ? (
    <ReferenceField { ...props } reference="users" source="userId" linkType="show">
      <FullNameField />
    </ReferenceField>
  ) : <TextField { ...props } source="userId" />
}

const AnalyticsFilter = props => (
  <Filter {...props}>
    <TextInput label="Search" source="q" alwaysOn />
    <ReferenceInput label="By User" source="userId" reference="users">
      <AutocompleteInput optionText="email" inputValueMatcher={() => null} />
    </ReferenceInput>
    <SelectInput label="Action" source="action" allowEmpty choices={[
      { id: 'Searched Providers', name: 'Searched Providers' },
      { id: 'Booked Appointment', name: 'Booked Appointment' }
    ]}/>
  </Filter>
)
const list = props => (
  <List {...props} filters={<AnalyticsFilter />} perPage={50} bulkActionButtons={false} title="All Analytics">
    <Datagrid rowClick="show">
      <OptionalUserField label="User" />
      <DateField source="createdAt" label="Date" />
      <TextField source="action" />
      <TextField source="detail" />
    </Datagrid>
  </List>
)

const show = props => (
  <Show {...props} title="View Analytics Action">
    <SimpleShowLayout>
      <TextField source="id" label="Action ID" />
      <OptionalField label="User" source="userId">
        <ReferenceField reference="users" linkType="show">
          <FullNameField />
        </ReferenceField>
      </OptionalField>
      <DateField source="createdAt" showTime label="Date" />
      <TextField source="action" />
      <TextField source="detail" />
      <UrlField source="link" target="_blank" rel="noopener noreferrer" />
    </SimpleShowLayout>
  </Show>
)

export default { list, show }