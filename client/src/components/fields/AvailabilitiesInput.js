import React, { Component } from 'react'
import { connect } from 'react-redux'
import { ArrayInput, SimpleFormIterator, TextInput, NumberInput, FormDataConsumer, Button,
  REDUX_FORM_NAME
} from 'react-admin'
import { change } from 'redux-form'
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import IconContentAdd from '@material-ui/icons/Today';
import IconContentRemove from '@material-ui/icons/Remove';
import IconContentRemoveCircle from '@material-ui/icons/RemoveCircleOutline';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { isArrayLength } from '../../utils/helpers';

const styles = theme => ({
  root: {
    width: '100%',
    marginTop: '20px'
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
  },
  arrayInput: {
    width: '100%',
    marginTop: '-25px'
  },
  panelSummary: {
    backgroundColor: 'rgba(0,0,0,.03)',
    borderBottom: '1px solid rgba(0,0,0,.125)',
    marginBottom: -1
  },
  panelDetails: {
    display: 'block'
  },
  generateButton: {
    margin: '20px 8px'
  }
});

const days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']

const defaultTimes = [
  { from: 9, to: 17 }
]

class AvailabilitiesInput extends Component {
  static propTypes = {
    classes: PropTypes.object.isRequired,
  };

  generateAvailabilities = () => {
    const { change } = this.props
    change(REDUX_FORM_NAME, 'availabilities', {
      0: defaultTimes,
      1: defaultTimes,
      2: defaultTimes,
      3: defaultTimes,
      4: defaultTimes,
      5: defaultTimes,
      6: defaultTimes
    })
  }

  clearAvailabilities = () => {
    const { change } = this.props
    change(REDUX_FORM_NAME, 'availabilities', null)
  }

  clearAvailabilitiesDay = day => {
    const { change } = this.props
    change(REDUX_FORM_NAME, `availabilities[${day}]`, [])
  }

  render(){
    const { classes } = this.props
    return (
      <FormDataConsumer>
        {({ formData }) => {
          const availabilities = formData && formData.availabilities
          return availabilities ? (
            <div className={classes.root}>
              { days.map((day, index) => (
                  <ExpansionPanel key={index}>
                    <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />} className={classes.panelSummary}>
                      <Typography className={classes.heading}>{ day } { availabilities[index] && !!availabilities[index].length && `(${availabilities[index].length})` }</Typography>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails className={classes.panelDetails}>
                      <ArrayInput className={classes.arrayInput} source={`availabilities[${index}]`} label="">
                        <SimpleFormIterator>
                          <NumberInput source="from" label="From" />
                          <NumberInput source="to" label="To" />
                        </SimpleFormIterator>
                      </ArrayInput>
                      { isArrayLength(availabilities[index]) &&
                        <Button color="default" onClick={() => this.clearAvailabilitiesDay(index)} label="Remove all">
                          <IconContentRemoveCircle />
                        </Button>
                      }
                    </ExpansionPanelDetails>
                  </ExpansionPanel>
              ))}
              <Button variant="outlined" onClick={this.generateAvailabilities} label="Reset Availabilities" className={classes.generateButton}>
                <IconContentAdd />
              </Button>
              <Button variant="outlined" color="secondary" onClick={this.clearAvailabilities} label="Clear Availabilities" className={classes.generateButton}>
                <IconContentRemove />
              </Button>
            </div>
          ) : (
            <Button variant="outlined" onClick={this.generateAvailabilities} label="Generate Default Availabilities" className={classes.generateButton}>
              <IconContentAdd />
            </Button>
          )
        }}
      </FormDataConsumer>
    )
  }
}

const mapStateToProps = () => ({})
const mapDispatchToProps = { change }

export default connect(mapStateToProps, mapDispatchToProps)(
  withStyles(styles)(AvailabilitiesInput)
)