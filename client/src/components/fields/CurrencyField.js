import React from 'react'
import { NumberField } from 'react-admin'

const CurrencyField = props => (
  <NumberField { ...props } options={{ style: 'currency', currency: 'USD' }} />
)

CurrencyField.defaultProps = {
  addLabel: true,
}

export default CurrencyField