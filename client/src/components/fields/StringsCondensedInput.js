import React, { Component } from 'react'
import { addField } from 'react-admin'
import ChipInput from 'material-ui-chip-input'

class StringsCondensedInput extends Component {
  handleAdd = chip => {
    const { input } = this.props
    input.onChange([...(input.value || []), chip])
  }

  handleDelete = chip => {
    const { input } = this.props
    input.onChange(input.value.filter(str => str !== chip));
  }

  render(){
    const { input, label, meta: { touched, error }} = this.props
    return (
      <ChipInput
        label={label}
        error={!!(touched && error)}
        helperText={touched && error}
        value={input.value || []}
        onAdd={this.handleAdd}
        onDelete={this.handleDelete} />
    )
  }
}

export default addField(StringsCondensedInput)