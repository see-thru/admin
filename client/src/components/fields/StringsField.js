import React from 'react'
import get from 'lodash/get'
import Typography from '@material-ui/core/Typography'

const StringsField = ({ record, source }) => {
  const strings = get(record, source)
  return (
    <div>
      { strings && strings.map((str, index) => (
        <Typography component="span" body1="body1" key={index}>
          {str}
        </Typography>
      ))}
    </div>
  )
}

StringsField.defaultProps = { addLabel: true }

export default StringsField