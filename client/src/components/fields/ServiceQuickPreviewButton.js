import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import Drawer from '@material-ui/core/Drawer';
import { withStyles } from '@material-ui/core/styles';

import IconImageEye from '@material-ui/icons/RemoveRedEye';
import IconKeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import { Button, SimpleShowLayout, TextField, NumberField, RichTextField, BooleanField } from 'react-admin';
import { CurrencyField, StringsField, OptionalField, OptionalArrayField, OptionalTextField } from '.'

const styles = theme => ({
  field: {
    // These styles will ensure our drawer don't fully cover our
    // application when teaser or title are very long
    '& span': {
      display: 'inline-block',
      maxWidth: '20em'
    }
  }
});

const ServicePreviewView = ({ classes, record, ...props }) => (
  <SimpleShowLayout record={record} {...props}>
    <TextField source="id" label="Service ID" />
    <TextField source="name" />
    <CurrencyField source="cost" />
    <NumberField source="time" />
    <OptionalField source="isDefault" label="Is Default"><BooleanField  /></OptionalField>
    <OptionalArrayField source="includes" label="Includes"><StringsField /></OptionalArrayField>
    <OptionalTextField source="adaCode" label="ADA Code" />
    <OptionalTextField source="cptCode" label="CPT Code" />
    <OptionalField source="description" label="description"><RichTextField /></OptionalField>
  </SimpleShowLayout>
);

const mapStateToProps = (state, props) => ({
  // Get the record by its id from the react-admin state.
  record: state.admin.resources[props.resource]
    ? state.admin.resources[props.resource].data[props.id]
    : null,
  version: state.admin.ui.viewVersion
});

const ServicePreview = connect(mapStateToProps, {})(
  withStyles(styles)(ServicePreviewView)
);

class ServiceQuickPreviewButton extends Component {
  state = { showPanel: false };

  // load it every time, so that it gets the edits? like ServiceQuickEditButton?
  // or use a reducer to update it..
  handleClick = () => {
    this.setState({ showPanel: true });
  };

  handleCloseClick = () => {
    this.setState({ showPanel: false });
  };

  render() {
    const { showPanel } = this.state;
    const { id } = this.props;
    return (
      <Fragment>
        <Button onClick={this.handleClick} label="ra.action.show">
          <IconImageEye />
        </Button>
        <Drawer
          anchor="right"
          open={showPanel}
          onClose={this.handleCloseClick}>
          <div>
            <Button label="Close" onClick={this.handleCloseClick}>
              <IconKeyboardArrowRight />
            </Button>
          </div>
          <ServicePreview id={id} basePath="/services" resource="services" />
        </Drawer>
      </Fragment>
    );
  }
}

export default connect()(ServiceQuickPreviewButton);
