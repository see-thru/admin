import React from 'react'
import PropTypes from 'prop-types';
import { ArrayField, Datagrid, NumberField } from 'react-admin'
import { withStyles } from '@material-ui/core';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

const styles = theme => ({
  root: {
    width: '100%',
    marginTop: '20px'
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
  },
  summary: {
    backgroundColor: 'rgba(0,0,0,.03)',
    borderBottom: '1px solid rgba(0,0,0,.125)',
    marginBottom: -1
  },
  emptySection: {
    margin: '20px'
  }
});

const days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']

const AvailabilitiesField = ({ record, classes }) => {
  const { availabilities } = record
  return availabilities ? (
    <div className={classes.root}>
      { days.map((day, index) => (
          <ExpansionPanel key={index}>
            <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />} className={classes.summary}>
              <Typography className={classes.heading}>{ day } { availabilities[index] && !!availabilities[index].length && `(${availabilities[index].length})` }</Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
              <ArrayField source={`availabilities[${index}]`} label="" record={record}>
                <Datagrid>
                  <NumberField source="from" label="From" />
                  <NumberField source="to" label="To" />
                </Datagrid>
              </ArrayField>
            </ExpansionPanelDetails>
          </ExpansionPanel>
      ))}
    </div>
  ) : <Typography className={classes.emptySection}>The default availabilities are being used.</Typography>
}

AvailabilitiesField.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(AvailabilitiesField)