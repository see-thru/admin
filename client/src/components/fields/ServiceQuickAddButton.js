import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { submit, isSubmitting, arrayPush, reset } from 'redux-form';
import {
  fetchEnd,
  fetchStart,
  showNotification,
  Button,
  SaveButton,
  BooleanInput,
  ReferenceInput,
  SimpleForm,
  TextInput,
  NumberInput,
  CREATE,
  REDUX_FORM_NAME
} from 'react-admin';

import RichTextInput from 'ra-input-rich-text'
import IconContentAdd from '@material-ui/icons/AddCircleOutline';
import IconCancel from '@material-ui/icons/Cancel';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';

import dataProvider from '../../api/dataProvider'
import { AutocompleteInput, StringsInput } from '.'

const quickAddFormName = 'service-quick-add'

const ServiceRef = ({ record, excludeIds, ...props }) => (
  <ReferenceInput { ...props } 
    record={record} 
    label="Service Reference"
    reference="services"
    filter={{ ids_nin: excludeIds }}
    allowEmpty>
    <AutocompleteInput optionText="name" inputValueMatcher={() => null} />
  </ReferenceInput>
)

class ServiceQuickAddButton extends Component {
  state = {
    error: false,
    showDialog: false
  };

  handleClick = () => {
    console.log('clicking')
    this.setState({ showDialog: true });
  };

  handleCloseClick = () => {
    console.log('close click')
    this.setState({ showDialog: false });
  };

  handleSaveClick = () => {
    console.log('handleSaveClick')
    const { submit } = this.props;

    // Trigger a submit of our custom quick create form
    // This is needed because our modal action buttons are outside the form
    submit(quickAddFormName);
  };

  // TODO: Error handling. Show errors in modal?
  handleSubmit = values => {
    console.log('handleSubmit', values)
    const { arrayPush, reset } = this.props
    if(values._id){
      console.log('Use Existing Service')
      arrayPush(REDUX_FORM_NAME, 'services', { _id: values._id });
      this.setState({ showDialog: false });
      reset(quickAddFormName)
    } else {
      console.log('Create New Service')
      const { fetchStart, fetchEnd, showNotification } = this.props;
  
      // Dispatch an action letting react-admin know a API call is ongoing
      fetchStart();
  
      // As we want to know when the new post has been created in order to close the modal, we use the
      // dataProvider directly
      dataProvider(CREATE, 'services', { data: values })
        .then(({ data }) => {
          console.log('created service', data, REDUX_FORM_NAME)
          // Update the main react-admin form (in this case, the comments creation form)
          arrayPush(REDUX_FORM_NAME, 'services', { _id: data.id });
          this.setState({ showDialog: false });
          reset(quickAddFormName)
        })
        .catch(error => {
          showNotification(error.message, 'error');
        })
        .finally(() => {
          // Dispatch an action letting react-admin know a API call has ended
          fetchEnd();
        });
    }
  };

  render() {
    const { showDialog } = this.state;
    const { isSubmitting, excludeIds } = this.props;
    return (
      <Fragment>
        <Button onClick={this.handleClick} label="ra.action.add">
          <IconContentAdd />
        </Button>
        <Dialog
          fullWidth
          open={showDialog}
          onClose={this.handleCloseClick}
          aria-label="Add Service"
        >
          <DialogTitle>Add Service to Provider</DialogTitle>
          <DialogContent>
            <SimpleForm // We override the redux-form name to avoid collision with the react-admin main form
              form={quickAddFormName}
              resource="services"
              // We override the redux-form onSubmit prop to handle the submission ourselves
              onSubmit={this.handleSubmit}
              // We want no toolbar at all as we have our modal actions
              toolbar={null}>
              <Fragment><h4 style={{marginBottom: 0}}>Use Existing Service:</h4></Fragment>
              <ServiceRef source="_id" excludeIds={excludeIds} />

              <Fragment>
                <hr />
                <h4 style={{marginBottom: 0}}>Create New Service:</h4>
              </Fragment>
              <TextInput source="name" />
              <NumberInput source="cost" />
              <NumberInput source="time" />
              <StringsInput source="includes" label="Includes" />
              <RichTextInput source="description" />
              <BooleanInput source="isDefault" />
            </SimpleForm>
          </DialogContent>
          <DialogActions>
            <SaveButton
              saving={isSubmitting}
              onClick={this.handleSaveClick}
            />
            <Button label="ra.action.cancel" onClick={this.handleCloseClick}>
              <IconCancel />
            </Button>
          </DialogActions>
        </Dialog>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  isSubmitting: isSubmitting(quickAddFormName)(state)
});

const mapDispatchToProps = {
  arrayPush,
  fetchEnd,
  fetchStart,
  showNotification,
  submit,
  reset
};

export default connect(mapStateToProps, mapDispatchToProps)(
  ServiceQuickAddButton
);