import React, { Component, Fragment } from 'react'
import { SimpleShowLayout, TextField, NumberField, ReferenceField } from 'react-admin'
import { CurrencyField, StringsField } from '.'
import { Typography } from '@material-ui/core';
import ServiceQuickPreviewButton from './ServiceQuickPreviewButton';
import { isArrayLength } from '../../utils/helpers';

const ServiceReferenceField = ({ record, basePath }) => (
  <ReferenceField source="_id" record={record} reference="services" basePath={basePath || '/providers'} linkType={false}>
    <TextField source="name" />
  </ReferenceField>
)

class ServicesField extends Component {
  shouldComponentUpdate(nextProps){
    return nextProps.record.services !== this.props.record.services
  }
  render(){
    const { services } = this.props.record
    return (
      <Fragment>
        { isArrayLength(services) ? services.map((service, index) => (
          <div key={index}>
            <SimpleShowLayout record={service}>
              <ServiceReferenceField />
              <ServiceQuickPreviewButton id={service._id} />
              { service.cost && <CurrencyField source="cost" /> }
              { service.time && <NumberField source="time" /> }
              { service.includes && <StringsField source="includes" /> }
            </SimpleShowLayout>
          </div>
        )) : <Typography>The services array is empty.</Typography>}
      </Fragment>
    )
  }
}

ServicesField.defaultProps = { addLabel: true }

export default ServicesField
