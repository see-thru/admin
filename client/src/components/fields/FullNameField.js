import React from 'react'
import Typography from '@material-ui/core/Typography';

export default ({ record, className }) => (
  <Typography component="span" body1="body1" className={className}>
    { record.firstName } { record.lastName }
  </Typography>
)