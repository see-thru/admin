import React, { cloneElement } from 'react'
import get from 'lodash/get'
import { TextField, Labeled } from 'react-admin'
import { isArrayLength } from '../../utils/helpers';

export const OptionalTextField = ({ label, ...props }) => {
  const value = get(props.record, props.source)
  return value ? (
    <Labeled label={label}>
      <TextField { ...props } />
    </Labeled> 
  ) : null
}

export const OptionalArrayField = ({ children, label, ...props }) => {
  const value = get(props.record, props.source)
  return isArrayLength(value) ? (
    <Labeled label={label}>
      { cloneElement(children, props) }
    </Labeled>
  ): null
}

export const OptionalField = ({ children, label, ...props }) => {
  const value = get(props.record, props.source)
  return value ? (
    <Labeled label={label}>
      {cloneElement(children, props)}
    </Labeled>
  ): null
}