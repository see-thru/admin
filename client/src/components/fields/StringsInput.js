import React from 'react'
import styled from 'styled-components'
import { FieldArray, Field } from 'redux-form'
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import CloseIcon from '@material-ui/icons/RemoveCircleOutline';
import AddIcon from '@material-ui/icons/AddCircleOutline';

const renderField = ({ input, label, meta: { touched, error }, ...custom }) => (
    <TextField
      label={label}
      error={!!(touched && error)}
      helperText={touched && error}
      {...input}
      {...custom} />
);

const StyledList = styled.ul`
  margin: 0;
  input {
    min-width: 300px;
    max-width: 50%;
  }
`
const renderArrayField = ({ fields, meta: { error } }) => (
  <StyledList>
    {fields.map((item, index) => (
      <li key={index}>
        <Field name={item} type="text" component={renderField} />
        <span>
          <Button
            className="button-remove"
            size="small"
            onClick={() => fields.remove(index)}>
            <CloseIcon /> &nbsp;Remove
          </Button>
        </span>
      </li>
    ))}
    <li>
      <span>
        <Button
          className="button-add"
          size="small"
          onClick={() => fields.push()}>
          <AddIcon /> &nbsp;Add
        </Button>
      </span>
    </li>
    {error && <li className="error">{error}</li>}
  </StyledList>
)

const StringsInput = ({ source }) => <FieldArray name={source} component={renderArrayField} />

StringsInput.defaultProps = { addLabel: true }

export default StringsInput