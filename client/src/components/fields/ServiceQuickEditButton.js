import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { submit, isSubmitting, change } from 'redux-form';
import {
  fetchEnd,
  fetchStart,
  required,
  showNotification,
  Button,
  SaveButton,
  SimpleForm,
  TextInput,
  NumberInput,
  DisabledInput,
  GET_ONE,
  UPDATE
} from 'react-admin';
import RichTextInput from 'ra-input-rich-text'
import IconContentEdit from '@material-ui/icons/Edit';
import IconCancel from '@material-ui/icons/Cancel';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import CircularProgress from '@material-ui/core/CircularProgress';
import dataProvider from '../../api/dataProvider'
import { StringsInput } from '.'

const quickEditFormName = 'service-quick-edit'

class ServiceQuickEditButton extends Component {
  state = {
    loading: false,
    error: '',
    service: {},
    showDialog: false
  };

  handleClick = async () => {
    // Perhaps only create one of the dialog components per page, and simply pass in the ID to the individual button's handleClick()? 
    const { service } = this.state
    this.setState({ showDialog: true })
    if(service.id) return
    try {
      this.setState({ loading: true });
      const response = await dataProvider(GET_ONE, 'services', { id: this.props.id })
      this.setState({ loading: false, error: '', service: response.data })
    } catch(e){
      console.error(e)
      this.setState({ error: 'There was an error loading the service', loading: false })
    }
  };

  handleCloseClick = () => {
    this.setState({ showDialog: false });
  };

  handleSaveClick = () => {
    const { submit } = this.props;
    // Trigger a submit of our custom quick create form
    // This is needed because our modal action buttons are outside the form
    submit(quickEditFormName);
  };

  handleSubmit = values => {
    const { fetchStart, fetchEnd, showNotification, id } = this.props;
    console.log(values)
    // Dispatch an action letting react-admin know a API call is ongoing
    fetchStart();

    dataProvider(UPDATE, 'services', { id, data: values })
    .then(({ data }) => {
      // TODO: If name was changed, we'll probably want to update it. But is that possible?
      // Remount the component or replace the array item with the identical one...?
      // change(REDUX_FORM_NAME, source, record)
      this.setState({ showDialog: false, service: {} }); // ?
      showNotification('The service was saved!');
    })
    .catch(error => {
      this.setState({ error: error.message })
    })
    .finally(() => { // Dispatch an action letting react-admin know a API call has ended
      fetchEnd();
    });
  };

  render() {
    const { showDialog, error, loading, service } = this.state;
    const { isSubmitting } = this.props;
    return (
      <Fragment>
        <Button onClick={this.handleClick} label="ra.action.edit">
          <IconContentEdit />
        </Button>
        <Dialog
          fullWidth
          open={showDialog}
          onClose={this.handleCloseClick}
          aria-label="Edit service"
        >
          <DialogTitle>Edit service</DialogTitle>
          <DialogContent>
            { error && <div>{ error }</div> }
            { loading ? <CircularProgress /> : (
              <SimpleForm
                // We override the redux-form name to avoid collision with the react-admin main form
                form={quickEditFormName}
                resource="services"
                onSubmit={this.handleSubmit}
                toolbar={null}
                record={service}
              >
                <DisabledInput source="id" label="Service ID" />
                <TextInput source="name" validate={required()} />
                <NumberInput source="cost" />
                <NumberInput source="time" />
                <StringsInput source="includes" label="Includes" />
                <RichTextInput source="description" />
              </SimpleForm>
            )}
          </DialogContent>
          <DialogActions>
            <SaveButton
              saving={isSubmitting}
              onClick={this.handleSaveClick}
            />
            <Button label="ra.action.cancel" onClick={this.handleCloseClick}>
              <IconCancel />
            </Button>
          </DialogActions>
        </Dialog>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  isSubmitting: isSubmitting(quickEditFormName)(state)
});

const mapDispatchToProps = {
  change,
  fetchEnd,
  fetchStart,
  showNotification,
  submit
};

export default connect(mapStateToProps, mapDispatchToProps)(
  ServiceQuickEditButton
);