import React, { Fragment } from 'react'
import { withStyles } from '@material-ui/core/styles';
import styled from 'styled-components'
import { ArrayInput, SimpleFormIterator, NumberInput, TextField, ReferenceField, FormDataConsumer } from 'react-admin'
import ServiceQuickAddButton from './ServiceQuickAddButton'
import ServiceQuickEditButton from './ServiceQuickEditButton'
import ServiceQuickPreviewButton from './ServiceQuickPreviewButton'
import { StringsCondensedInput } from '.'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableRow from '@material-ui/core/TableRow'

// TODO: Stop using styled-components, and use themes/classes
const StyledArrayInput = styled(ArrayInput)`
  width: 100% !important;
  li { padding: 20px 0 7px; }
`
const StyledServiceReferenceInput = styled.div`
  // display: flex;
  // align-items: center;
  > span {
    padding: 10px 10px 10px 0;
  }
  .service-name {
    font-weight: 700;
  }
`

const ServiceReferenceInput = ({ record, basePath, source }) => {
  return record && record._id ? (
    <StyledServiceReferenceInput>
      <ReferenceField source="_id" record={record} reference="services" basePath={basePath || '/providers'} linkType={false}>
        <TextField source="name" className="service-name" />
      </ReferenceField>
      <div>
        <ServiceQuickPreviewButton id={record._id} />
        <ServiceQuickEditButton id={record._id} source={source} record={record} />
      </div>
    </StyledServiceReferenceInput>
  ) : null
}

const styles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
    overflowX: 'auto',
  },
  table: {
    minWidth: 700,
  },
  tableCell: {
    border: 'none',
    padding: '0 5px'
  }
});

const ServiceRow = ({ classes, source, label, ...props }) => {
  return (
    // For some reason, props injection isn't working correctly, and any edits are happening to all the services..
    <Table className={classes.table}>
      <TableBody>
        <TableRow>
          <TableCell component="th" scope="row" className={classes.tableCell}>
            <FormDataConsumer { ...props } source={source}>
              {({ scopedFormData, id }) =>
                <ServiceReferenceInput record={scopedFormData} basePath={props.basePath} source={id} />
              }
            </FormDataConsumer>
          </TableCell>
          <TableCell className={classes.tableCell}><NumberInput { ...props } source="cost" label="Cost" /></TableCell>
          <TableCell className={classes.tableCell}><NumberInput { ...props } source="time" label="Time" /></TableCell>
          <TableCell className={classes.tableCell}><StringsCondensedInput { ...props } source="includes" label="Includes" /></TableCell>
        </TableRow>
      </TableBody>
    </Table>
  )
}

const StyledServiceRow = withStyles(styles)(ServiceRow)

const ServicesInput = props => {
  return (
    <Fragment>
      <StyledArrayInput source="services" { ...props }>
        <SimpleFormIterator disableAdd>
          {/* <StyledServiceRow /> */}
          <FormDataConsumer>
            {({ scopedFormData, id }) =>
              <ServiceReferenceInput record={scopedFormData} basePath={props.basePath} source={id} />
            }
          </FormDataConsumer>
          <NumberInput source="cost" label="Cost" />
          <NumberInput source="time" label="Time" />
          <StringsCondensedInput source="includes" label="Includes" />
        </SimpleFormIterator>
      </StyledArrayInput>
      
      <FormDataConsumer>
        {({ formData }) => {
          const serviceIds = formData && formData.services ? formData.services.map(s => s._id).join('|') : ''
          return <ServiceQuickAddButton excludeIds={serviceIds} />
        }}
      </FormDataConsumer>
    </Fragment>
  )
}

export default ServicesInput;