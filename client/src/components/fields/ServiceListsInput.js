import React from 'react'
import styled from 'styled-components'
import { FieldArray, Field } from 'redux-form'
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import CloseIcon from '@material-ui/icons/RemoveCircleOutline';
import AddIcon from '@material-ui/icons/AddCircleOutline';
import { Button as AdminButton } from 'react-admin';
import ImageEye from '@material-ui/icons/RemoveRedEye';
import { Link } from 'react-router-dom';

const renderField = ({ input, label, meta: { touched, error }, ...custom }) => (
  <span>
    <TextField
      label={label}
      error={!!(touched && error)}
      helperText={touched && error}
      {...input}
      {...custom} />
    <AdminButton component={Link} to={`/serviceLists/${input.value}/show`} label="View" target="_blank"><ImageEye /></AdminButton>
  </span>
);

const StyledList = styled.ul`
  margin: 0;
  input {
    min-width: 300px;
    max-width: 50%;
  }
`
const renderArrayField = ({ fields, meta: { error } }) => (
  <StyledList>
    {fields.map((item, index) => (
      <li key={index}>
        <Field name={item} type="text" component={renderField} />        
        <Button size="small" onClick={() => fields.remove(index)}>
          <CloseIcon /> &nbsp;Remove
        </Button>
      </li>
    ))}
    <li>
      <span>
        <Button size="small" onClick={() => fields.push()}>
          <AddIcon /> &nbsp;Add
        </Button>
      </span>
    </li>
    {error && <li className="error">{error}</li>}
  </StyledList>
)

const ServiceListsInput = ({ source }) => <FieldArray name={source} component={renderArrayField} />

ServiceListsInput.defaultProps = { 
  addLabel: true,
  label: 'Service Lists'
}

export default ServiceListsInput