import React from 'react'
import get from 'lodash/get'
import Typography from '@material-ui/core/Typography'

const UrlsField = ({ record, source }) => {
  const strings = get(record, source)
  return (
    <div>
      { strings && strings.map((str, index) => (
        <Typography component="span" body1="body1" key={index}>
          <a href={str} target="_blank" rel="noopener noreferrer">{ str }</a>
        </Typography>
      ))}
    </div>
  )
}

UrlsField.defaultProps = { addLabel: true }

export default UrlsField