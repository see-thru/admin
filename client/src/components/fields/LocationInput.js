import React from 'react'
import { NumberInput } from 'react-admin'

const LocationInput = () => (
	<span>
		<NumberInput source="location.coordinates[1]" label="Latitude" disabled />
		&nbsp;
		<NumberInput source="location.coordinates[0]" label="Longitude" disabled />
	</span>
);

export default LocationInput