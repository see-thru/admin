import React from 'react'
import get from 'lodash/get'
import Typography from '@material-ui/core/Typography'
import { isArrayLength } from '../../utils/helpers'

const StringsCondensedField = ({ record, source }) => {
  const strings = get(record, source)
  return (
    <Typography component="span" body1="body1">
      {isArrayLength(strings) && strings.join(', ')}
    </Typography>
  )
}

StringsCondensedField.defaultProps = { addLabel: true }

export default StringsCondensedField