import React from 'react'
import { List, Datagrid, TextField, DisabledInput, EmailField,
  Edit, SimpleForm, TextInput, DateField, FunctionField, Show, SimpleShowLayout
} from 'react-admin'
import { SearchFilter, OptionalTextField, OptionalField } from '../fields'

const renderFullName = record => <span>{ record.firstName } { record.lastName }</span>

const list = props => (
  <List {...props} filters={<SearchFilter />} perPage={50} title="All Users">
    <Datagrid rowClick="show">
      <FunctionField label="Name" render={renderFullName} />
      <EmailField source="email" />
      <TextField source="invitedBy" />
    </Datagrid>
  </List>
);

const PageTitle = ({ record }) => {
  return <span>User{record ? `: ${record.firstName} ${record.lastName}` : ''}</span>;
};

const edit = props => (
  <Edit {...props} title={<PageTitle />}>
    <SimpleForm redirect="show">
      <DisabledInput source="id" label="User ID" />
      <TextInput source="firstName" />
      <TextInput source="lastName" />
      <DisabledInput source="email" />
      <TextInput source="invitedBy" />
      <TextInput source="dob" label="Date of Birth" />
      <TextInput source="phone" />
    </SimpleForm>
  </Edit>
);

const show = props => (
  <Show {...props} title={<PageTitle />}>
    <SimpleShowLayout>
      <TextField source="id" label="User ID" />
      <TextField source="firstName" />
      <TextField source="lastName" />
      <EmailField source="email" />
      <TextField source="invitedBy" />
      <TextField source="dob" label="Date of Birth" />
      <OptionalTextField source="phone" label="Phone" />
      <DateField source="createdAt" showTime />
      <OptionalField source="updatedAt" label="Updated At"><DateField showTime /></OptionalField>
      <OptionalTextField source="updatedBy" label="Updated By" />
    </SimpleShowLayout>
  </Show>
)

export default { list, edit, show }