import React from 'react'
import { List, Datagrid, TextField, NumberField, EditButton } from 'react-admin'
import { CurrencyField, SearchFilter } from '../fields'

export default props => (
  <List {...props} perPage={50} filters={<SearchFilter />} title="All Services">
    <Datagrid rowClick="show">
      <TextField source="name" />
      <CurrencyField source="cost" />
      <NumberField source="time" />
      <TextField source="category" />
      <EditButton onClick={e => e.stopPropagation()} />
    </Datagrid>
  </List>
);