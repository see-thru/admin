import React from 'react'
import { DisabledInput, Create, Edit, SimpleForm, TextInput,
  NumberInput, BooleanInput
} from 'react-admin'
import RichTextInput from 'ra-input-rich-text'
import { StringsInput } from '../fields'
import PageTitle from './page-title'

export const edit = props => (
  <Edit {...props} title={<PageTitle />}>
    <SimpleForm redirect="show">
      <DisabledInput source="id" label="Service ID" />
      <TextInput source="name" />
      <NumberInput source="cost" />
      <NumberInput source="time" />
      <StringsInput source="includes" label="Includes" />
      <BooleanInput source="isDefault" label="Is Default" />
      <TextInput source="category" />
      <TextInput source="adaCode" label="ADA Code" />
      <TextInput source="cptCode" label="CPT Code" />
      <RichTextInput source="description" />
    </SimpleForm>
  </Edit>
);

export const create = props => (
  <Create {...props}>
    <SimpleForm redirect="show">
      <TextInput source="name" />
      <NumberInput source="cost" />
      <NumberInput source="time" />
      <StringsInput source="includes" label="Includes" />
      <BooleanInput source="isDefault" label="Is Default" />
      <TextInput source="category" />
      <TextInput source="adaCode" label="ADA Code" />
      <TextInput source="cptCode" label="CPT Code" />
      <RichTextInput source="description" />
    </SimpleForm>
  </Create>
);