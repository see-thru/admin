import React from 'react'
import { TextField, NumberField, Show, SimpleShowLayout, RichTextField, BooleanField, DateField,
  EditButton, CloneButton
} from 'react-admin'
import { CurrencyField, StringsField, OptionalArrayField, OptionalTextField, OptionalField } from '../fields'
import PageTitle from './page-title'
import CardActions from '@material-ui/core/CardActions';

const ServiceShowActions = ({ basePath, data }) => (
  <CardActions style={{ zIndex: 2, display: 'inline-block', float: 'right' }}>
    <EditButton basePath={basePath} record={data} />
    <CloneButton basePath={basePath} record={data}  />
  </CardActions>
);

export default props => (
  <Show {...props} title={<PageTitle />} actions={<ServiceShowActions />}>    
    <SimpleShowLayout>
      <TextField source="id" label="Service ID" />
      <TextField source="name" />
      <CurrencyField source="cost" />
      <NumberField source="time" />
      <OptionalArrayField source="includes" label="Includes"><StringsField /></OptionalArrayField>
      <OptionalField source="isDefault" label="Is Default"><BooleanField /></OptionalField>
      <OptionalTextField source="category" label="Category" />
      <OptionalTextField source="adaCode" label="ADA Code" />
      <OptionalTextField source="cptCode" label="CPT Code" />
      <OptionalField source="description" label="description"><RichTextField /></OptionalField>
      
      <DateField source="createdAt" showTime />
      <OptionalField source="updatedAt" label="Updated at"><DateField showTime /></OptionalField>
      <OptionalTextField source="updatedBy" label="Updated by" />
    </SimpleShowLayout>
  </Show>
);