import { create, edit } from './edit'
import list from './list'
import show from './show'

export default { list, show, edit, create }