import React from 'react'

export default ({ record }) => {
  return <span>Service{record ? `: ${record.name}` : ''}</span>;
};