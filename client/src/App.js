import React from 'react';
import { Admin, Resource } from 'react-admin'
import providers from './components/providers'
import users from './components/users/index'
import appointments from './components/appointments'
import services from './components/services/index' // why isn't it finding the index.js?
import serviceLists from './components/serviceLists'
import analytics from './components/analytics'
import dataProvider from './api/dataProvider'
import Login from './components/login'
import MyLayout from './components/layout'
import UsersIcon from '@material-ui/icons/Group'
import CityIcon from '@material-ui/icons/LocationCity'
import ServicesIcon from '@material-ui/icons/MonetizationOn'
import AppointmentsIcon from '@material-ui/icons/DateRange'
import ListsIcon from '@material-ui/icons/ViewList'
import AnalyticsIcon from '@material-ui/icons/DeveloperMode'

import GlobalStyle from './styles/global'
import createHistory from 'history/createBrowserHistory'

const history = createHistory()
const App = ({ isLoggedIn }) => (
  <div className="full-height-container">
    <GlobalStyle />
    { isLoggedIn ? 
      <Admin dataProvider={dataProvider} appLayout={MyLayout} history={history}>
        <Resource name="providers" { ...providers } icon={CityIcon} />
        <Resource name="services" { ...services } icon={ServicesIcon} />
        <Resource name="serviceLists" { ...serviceLists } icon={ListsIcon} options={{ label: 'Service Lists' }} />
        <Resource name="users" { ...users } icon={UsersIcon} />
        <Resource name="appointments" { ...appointments } icon={AppointmentsIcon} />
        <Resource name="actions" { ...analytics } icon={AnalyticsIcon} options={{ label: 'Analytics' }} />
      </Admin> : <Login />
    }
  </div>
);

export default App