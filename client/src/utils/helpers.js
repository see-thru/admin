
export const isArrayLength = arr => Array.isArray(arr) && !!arr.length

export const alphabetize = values => values.sort()