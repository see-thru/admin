import Cookies from 'js-cookie'
import { stringify } from 'query-string';
import {
	fetchUtils,
	GET_LIST,
	GET_ONE,
	GET_MANY,
	GET_MANY_REFERENCE,
	CREATE,
	UPDATE,
	UPDATE_MANY,
	DELETE,
	DELETE_MANY,
} from 'react-admin';

/**
 * Maps react-admin queries to a json-server powered REST API
 *
 * @see https://github.com/typicode/json-server
 * @example
 * GET_LIST     => GET http://my.api.url/posts?_sort=title&_order=ASC&_start=0&_end=24
 * GET_ONE      => GET http://my.api.url/posts/123
 * GET_MANY     => GET http://my.api.url/posts/123, GET http://my.api.url/posts/456, GET http://my.api.url/posts/789
 * UPDATE       => PUT http://my.api.url/posts/123
 * CREATE       => POST http://my.api.url/posts/123
 * DELETE       => DELETE http://my.api.url/posts/123
 */
/**
 * @param {String} type One of the constants appearing at the top if this file, e.g. 'UPDATE'
 * @param {String} resource Name of the resource to fetch, e.g. 'posts'
 * @param {Object} params The data request params, depending on the type
 * @returns {Object} { url, options } The HTTP request parameters
 */
const apiUrl = '/api'
const httpClient = fetchUtils.fetchJson

const convertDataRequestToHTTP = (type, resource, params) => {
	let url = '';
	const options = {};
	switch (type) {
		case GET_LIST: {
			const { page, perPage } = params.pagination;
			const { field, order } = params.sort;
			const query = {
				...fetchUtils.flattenObject(params.filter),
				_skip: (page - 1) * perPage,
				_limit: perPage,
				_sort: field,
				_order: order
			};
			url = `${apiUrl}/${resource}?${stringify(query)}`;
			break;
		}
		case GET_ONE:
			url = `${apiUrl}/${resource}/${params.id}`;
			break;
		case GET_MANY_REFERENCE: {
			const { page, perPage } = params.pagination;
			const { field, order } = params.sort;
			const query = {
				...fetchUtils.flattenObject(params.filter),
        [params.target]: params.id, // ?
        _skip: (page - 1) * perPage,
				_limit: perPage,
				_sort: field,
				_order: order
			};
			url = `${apiUrl}/${resource}?${stringify(query)}`;
			break;
		}
		case UPDATE:
			url = `${apiUrl}/${resource}/${params.id}`;
			options.method = 'PUT';
			options.body = JSON.stringify(params.data);
			break;
		case CREATE:
			url = `${apiUrl}/${resource}`;
			options.method = 'POST';
			options.body = JSON.stringify(params.data);
			break;
		case DELETE:
			url = `${apiUrl}/${resource}/${params.id}`;
			options.method = 'DELETE';
			break;
		case GET_MANY: {
			const query = {
				ids_in: params.ids.join('|'),
			};
			url = `${apiUrl}/${resource}?${stringify(query)}`;
			break;
		}
		case DELETE_MANY: {
			url = `${apiUrl}/${resource}/deleteMany`;
			options.method = 'POST';
			options.body = JSON.stringify({ ids: params.ids });
			break;
		}
		default:
			throw new Error(`Unsupported fetch action type ${type}`);
	}
	return { url, options };
};

/**
 * @param {Object} response HTTP response from fetch()
 * @param {String} type One of the constants appearing at the top if this file, e.g. 'UPDATE'
 * @param {String} resource Name of the resource to fetch, e.g. 'posts'
 * @param {Object} params The data request params, depending on the type
 * @returns {Object} Data response
 */
const convertHTTPResponse = (response, type, resource, params) => {
	const { headers, json } = response;
	switch (type) {
		case GET_LIST:
		case GET_MANY_REFERENCE:
			if (!headers.has('x-total-count')) {
				throw new Error(
					'The X-Total-Count header is missing in the HTTP Response. The jsonServer Data Provider expects responses for lists of resources to contain this header with the total number of results to build the pagination. If you are using CORS, did you declare X-Total-Count in the Access-Control-Expose-Headers header?'
				);
			}
			return {
				data: json,
				total: parseInt(
					headers
						.get('x-total-count')
						.split('/')
						.pop(),
					10
				),
			};
		case CREATE:
			return { data: { ...params.data, id: json.id } };
		default:
			return { data: json };
	}
};

/**
 * @param {string} type Request type, e.g GET_LIST
 * @param {string} resource Resource name, e.g. "posts"
 * @param {Object} payload Request parameters. Depends on the request type
 * @returns {Promise} the Promise for a data response
 */
export default async (type, resource, params) => {
	try {
		// if((type === GET_LIST) && (params.pagination.perPage === -1)){
		// 	return Promise.resolve({ data: [], total: 0 })
		// }
		// if(type === UPDATE && resource === 'providers') {
		// 	// https://marmelab.com/react-admin/DataProviders.html#decorating-your-rest-client-example-of-file-upload
		// 	const { files } = params.data
		// 	console.log('saving provider', params.data)
		// 	if(files && files.length){
		// 		const formerFiles = files.filter(p => !(p.rawFile instanceof File));
		// 		const newFiles = files.filter(p => p.rawFile instanceof File);
		// 		if(newFiles.length){
		// 			console.log('uploading files', newFiles)
		// 			// uploadedFiles = [{ src, title }]
		// 			const uploadedFiles = await Promise.all(newFiles.map(uploadFile))
		// 			params.data.files = [ ...uploadedFiles, ...formerFiles ]
		// 		}
		// 	}
		// }
		
		if (type === UPDATE_MANY) { // TODO: No need. Do this in the server.
			const responses = await Promise.all(
				params.ids.map(id =>
					httpClient(`${apiUrl}/${resource}/${id}`, {
						method: 'PUT',
						body: JSON.stringify(params.data),
					})
				)
			)
			return { data: responses.map(response => response.json) }
		}
	
		const { url, options } = convertDataRequestToHTTP(type, resource, params);
	
		const response = await httpClient(url, options)
		return convertHTTPResponse(response, type, resource, params)
	} catch(error) {
		if(error.status === 403){
			Cookies.remove('st-access')
			window.location.reload()
		} else {
			console.error(error.body ? error.body : error)
			return Promise.reject(error.body ? error.body.error : error.message)
		}
	}
};

// function uploadFile(file) {
// 	// can pass the file info to the backend and use the cloudinary sdk.... or call the cloudinary api manually, with proper auth...?
// 	return new Promise((resolve, reject) => {
// 		// our formdata
// 		const formData = new FormData();
// 		formData.append("file", file);
// 		formData.append("tags", '{TAGS}'); // Add tags for the images - {Array}
// 		formData.append("upload_preset", "{YOUR_PRESET}"); // Replace the preset name with your own
// 		formData.append("api_key", "{YOUR_API_KEY}"); // Replace API key with your own Cloudinary API key
// 		formData.append("timestamp", (Date.now() / 1000) | 0);

// 		// Replace cloudinary upload URL with yours
// 		return axios.post(
// 			"https://api.cloudinary.com/v1_1/{CLOUD_NAME}/image/upload",
// 			formData, 
// 			{ headers: { "X-Requested-With": "XMLHttpRequest" }})
// 			.then(response => console.log(response.data))

// 		cloudinary.v2.uploader.upload(file.src, { use_filename: true, folder: 'Provider Files' }, (error, result) => {
// 			if(error) reject(error)
// 			else {
// 				console.log('File uploaded', result)
// 				file.src = result.url
// 				file.public_id = result.public_id
// 				resolve(file)
// 			}
// 		});
// 	})
// }