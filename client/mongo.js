// this is just scrap paper for various mongo commands

var p = {
	"_id" : ObjectId("5c081c7e2f7fc8afb6c5fd97"),
	"providerInfo" : {
		"name" : "Limor Weinstein",
		"practiceName" : "LW Wellness Network",
		"degrees" : "MA, MHC, LP, FAED",
		"rating" : 95,
		"specialty" : "Wellness",
		"phoneNumber" : "917-670-9780",
		"email" : "limor.w@limorweinstein.com",
		"description" : "As a mother of three, wife and psychotherapist, Limor has dedicated her life to helping others navigate their way through the personal challenges and behaviors that inhibit happiness and harmony in their lives.\n\nOriginally from Israel, Limor credits her experience living on a Kibbutz as an adolescent, during her own family struggles, along with her service as a commander in the Israeli military, with giving her a strong sense of purpose, discipline and values. Twenty years of experience in childcare fueled her desire to promote healthy, collaborative care for individuals and families in need of guidance. Limor’s approach has a strong focus on openness, communication and positive feedback. Along with her team of professionals, she is committed to cultivating happier, healthier lifestyles and stronger more productive relationships between family members.",
		"languages" : [
			"English",
			"Hebrew"
		],
		"education" : [
			"Two Masters degrees in Mental Health Counseling, from Columbia University and City College",
			" Mental Health Counselor for Behavior Therapy of New York and a Counselor for CSAB"
		],
		"photo" : "https://cdn4.sussexdirectories.com/rms/rms_photos/sized/36/67/336736-929644-3_320x400.jpg?pu=1513369538"
	},
	"address" : "New York, New York 10075",
	"location" : {
		"type" : "Point",
		"coordinates" : [
			-73.987156,
			40.730862
		]
	},
	"services" : [
		{
			"_id" : ObjectId("5c081a902f7fc8afb6c5fd94")
		},
		{
			"_id" : ObjectId("5c081a902f7fc8afb6c5fd95")
		}
	],
	"createdOn" : ISODate("2018-12-05T18:44:14.911Z"),
	"updatedBy" : "Mark@seethru.healthcare",
	"updatedOn" : ISODate("2018-12-05T18:54:59.966Z")
}

var p2 = {
	"_id" : ObjectId("5c081e6e92068613b67ea0c1"),
	"providerInfo" : {
		"name" : "Kanchan Katapadi",
		"practiceName" : "Park Slope Medical Office",
		"degrees" : "MD",
		"rating" : 94,
		"specialty" : "Internal Medicine",
		"phoneNumber" : "718-832-1964",
		"npi" : "1215999362",
		"description" : "Dr. Kanchan Katapadi is an accomplished internist with high sensitivity to patient needs and strong research experience. Dr. Katapadi serves Brooklyn from her shared private practice, where services include physicals, same-day sick visits, minimally-invasive surgery, and other procedures. She strives to make patient visits comfortable and efficient as possible, taking care to listen to patient needs and concerns. \n    \n    Proudly serving the Brooklyn area for about 15 years, Dr. Katapadi originally attended medical school at Bangalore Medical College and Research Institute. She came to New York for her residency in internal medicine at New York Methodist Hospital. With a hunger for more training, she pursued a second residency at Nassau University Medical Center in Physical Medicine and Rehabilitation. Some of Dr. Katapadi's important co-research includes “Pulmonary Botryomycosis in a Patient with AIDS,” as published in Chest in 1996, and “Analysis of Smear Negative Tuberculosis,” which appeared in in Journal of Investigative Medicine in 1995. \n    \n    In her off-time, Dr. Katapadi loves to bond with her three children. Her hobbies include reading and traveling, and she stays active by skiing and hiking. Dr. Katapadi is certified by the American Board of Internal Medicine.",
		"languages" : [
			"English"
		],
		"education" : [
			"Board certifications",
			"American Board of Internal Medicine",
			"Hospice and Palliative Medicine (Internal Medicine)",
			"Education and training",
			"Medical School - Bangalore Medical College and Research Institute, Bachelor of Medicine, Bachelor of Surgery",
			"New York Methodist Hospital, Residency in Internal Medicine",
			"Nassau University Medical Center, Residency in Physical Medicine and Rehabilitation"
		],
		"photo" : "https://dsw5h1xg5uvx.cloudfront.net/b634b95f-fb6a-447c-8f81-cc113aa032cezoom.jpg"
	},
	"address" : "264 1st St Fl 2, Brooklyn, NY 11215",
	"location" : {
		"type" : "Point",
		"coordinates" : [
			-73.983936,
			40.674662
		]
	},
	"services" : [
		{
			"_id" : ObjectId("5c081a902f7fc8afb6c5fd96")
		}
	],
	"createdOn" : ISODate("2018-12-05T18:52:30.844Z")
}

db.actions.find({link: { $regex: '#/appointments' }}, { link: 1 }).limit(100).forEach(a => {
	const newLink = a.link.replace('#/','') + '/show'
	db.actions.update({ _id: a._id }, { $set: { link: newLink }})
})

db.providers.find({ createdOn: { $exists: true }}, { createdOn: 1 }).limit(20).forEach(p => {
	db.providers.update({ _id: p._id }, {
		$set: { createdAt: p.createdOn },
		$unset: { createdOn: 1 }
	})
})
db.providers.count({ usState: { $exists: true }})

// todo: string -> array
db.providers.find({ usState: { $exists: false }}).limit(100).forEach(p => {
	if(p && p.address){
		var reg = p.address.match(/, ([A-Z][A-Z]) \d\d\d\d\d$/);
		if(reg && reg[1]){
			db.providers.updateOne({ _id: p._id }, { $set: {
				usState: [ reg[1] ]
			}});
		}
	}
});

db.providers.update({}, { $unset: { usState: 1 }}, { multi: true })

db.students.updateMany( {}, { $rename: { "usState": "usStates" } } )