In /client: 
`yarn deploy` to build React app and deploy to Google App Engine

In /api:
`gcloud app deploy` to deploy to Google App Engine

In root directory:
`gcloud app deploy dispatch.yaml` to instruct App Engine to use API service for all `/api` routes